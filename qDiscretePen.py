#! /usr/bin/env python
import numpy as np
import math
import random
import gym
import cPickle as pkl
import matplotlib.pyplot as plt
from model import discretePendulum
from utility import centralSlice, getIndex

## Initialize the "pendulum" environment
env = discretePendulum(False)
# env = gym.make('Pendulum-v0')

## Defining the environment related constants

# Number of discrete states (bucket) per state dimension
NUM_BUCKETS = (126, 63)  # (theta, theta')
NUM_ACTIONS = env.action_space.n
NUM_ACTION_BUCKETS = (3,)  # make it smaller
# Bounds for each discrete state
LOW_BOUNDS = [0.0, -3.0]
HIGH_BOUNDS = [2*math.pi, 3.0]
# Index of the action
ACTION_INDEX = len(NUM_BUCKETS)

## Creating a Q-Table for each state-action pair
q_table = np.ones(NUM_BUCKETS + NUM_ACTION_BUCKETS) * -100
vSlice = [centralSlice(low, high, bucket) for low, high, bucket in zip(LOW_BOUNDS, HIGH_BOUNDS, NUM_BUCKETS)]
## Learning related constants
# MIN_EXPLORE_RATE = 0.01
epsilonDecay = 0.95
minEpsilon = 0.001
MIN_LEARNING_RATE = 0.1

## Defining the simulation related constants
NUM_EPISODES = 3000
MAX_T = 500
STREAK_TO_END = 120
SOLVED_T = 199
DEBUG_MODE = False

def main(args):
    if args.learn:
        learn(args.cont)
    if args.show:
        show()
    if args.plot:
        plotQ()

def rewardFun(x):
    if x.ndim == 1:
        return -(x[0] - np.pi)**2 - 0.25*x[1]**2
    else:
        return -(x[:, 0] - np.pi) ** 2 - 0.25 * x[:, 1] ** 2


def learn(cont):
    global q_table
    EXPLORE_RATE = 0.1
    np.random.rand(1337)
    mat1, mat2 = np.meshgrid(vSlice[0], vSlice[1], indexing='ij')
    pair = np.stack((mat1.flatten(), mat2.flatten()), axis=1)
    R = np.reshape(rewardFun(pair), NUM_BUCKETS)
    if cont:
        with open('qPenTable.pkl', 'rb') as f:
            q_table = pkl.load(f)
        EXPLORE_RATE = 0.05
    else:
        for k in range(q_table.shape[2]):
            q_table[:, :, k] = R  # + 0.01*np.random.normal(size=NUM_BUCKETS)
    EXPLORE_RATE = 0.0
    ## Instantiating the learning related parameters
    learning_rate = get_learning_rate(0)
    # explore_rate = get_explore_rate(0)
    discount_factor = 0.9  # since the world is unchanging
    targetind = getIndex(vSlice, np.array([np.pi, 0]))
    q_table[targetind] = 0

    num_streaks = 0

    for episode in range(NUM_EPISODES):
        q_prev = q_table.copy()
        # Reset the environment
        obv = env.reset()

        # the initial state
        ind0 = getIndex(vSlice, obv)

        for t in range(MAX_T):
            # env.render()

            # Select an action
            actind = select_action(ind0, EXPLORE_RATE)

            # Execute the action
            obv, reward, done, _ = env.step(actind[0])
            # Setting up for the next iteration
            ind1 = getIndex(vSlice, obv)
            # if ind1[0] == targetind[0] and ind1[1] == targetind[1]:
            if np.linalg.norm([obv[0] - np.pi, obv[1]]) < 0.01:
                bonus = 100
                done = True
            else:
                bonus = 0
            reward = R[ind1]

            # Update the Q based on the result
            best_q = np.amax(q_table[ind1])
            q_table_ind = tuple(np.append(ind0, actind).tolist())
            q_table[q_table_ind] += learning_rate*(reward + discount_factor*(best_q) - q_table[q_table_ind] + bonus)
            # update state by updating ind
            ind0 = ind1
            # Print data
            if (DEBUG_MODE):
                print("\nEpisode = %d" % episode)
                print("t = %d" % t)
                print("Action: %d" % actind[0])
                print("Obs: %s" % str(obv))
                print("Reward: %f" % reward)
                print("Best Q: %f" % best_q)
                print("Explore rate: %f" % EXPLORE_RATE)
                print("Learning rate: %f" % learning_rate)
                print("Streaks: %d" % num_streaks)

                print("")

            if done:
               print("Episode %d finished after %f time steps" % (episode, t))
               break

            #sleep(0.25)
        # Update parameters
        # explore_rate = get_explore_rate(episode)
        if EXPLORE_RATE > minEpsilon:
            EXPLORE_RATE *= epsilonDecay
        learning_rate = get_learning_rate(episode)
        # print change of q
        dq = q_table - q_prev
        ddq = [np.linalg.norm(dq[:, :, i], ord=np.inf) for i in range(NUM_ACTIONS)]
        print('episode {} ddq {}'.format(episode, ddq))
        if np.linalg.norm(np.array(ddq)) < 1e-4:
            print('Change of q is so small that we consider as convergence')
            break
    with open('qPenTable.pkl', 'wb') as f:
        pkl.dump(q_table, f)


def show(numTrial=100):
    global q_table
    with open('qPenTable.pkl', 'rb') as f:
        q_table = pkl.load(f)
    """Use the qtable we learned and show how it performs"""
    for i in range(numTrial):
        obv = env.reset()
        obv0 = obv
        # the initial state
        ind0 = getIndex(vSlice, obv)

        for t in range(MAX_T):
            env.render()

            # Select an action
            actind = select_action(ind0, 0.0)  # always use optimal q

            # Execute the action
            obv, reward, done, _ = env.step(actind[0])

            # Setting up for the next iteration
            ind0 = getIndex(vSlice, obv)

            # Print data
            if (DEBUG_MODE):
                print("t = %d, state = %s, action = %s" % (t, str(obv), str(actind)))

            if np.linalg.norm([obv[0] - np.pi, obv[1]]) < 0.05:
                done = True

            if done:
                break
        print('Simulation {} from {} to {}'.format(i, obv0, obv))


def plotQ():
    global q_table
    with open('qPenTable.pkl', 'rb') as f:
        q_table = pkl.load(f)
    """Use the qtable we learned and plot the policy function"""
    thslice, thdotslice = vSlice
    thX, thdotY = np.meshgrid(thslice, thdotslice, indexing='ij')
    mat = np.zeros(NUM_BUCKETS)
    for i in range(NUM_BUCKETS[0]):
        for j in range(NUM_BUCKETS[1]):
            tb = q_table[i, j]
            uind = np.argmax(tb)
            if uind == 0:
                u = -1
            elif uind == 1:
                u = 0
            else:
                u = 1
            mat[i, j] = u
    # plot the optimal control
    fig, ax = plt.subplots()
    cax = ax.contourf(thX, thdotY, mat, cmap=plt.cm.jet)
    ax.set_xlabel(r'$\theta$')
    ax.set_ylabel(r'$\omega$')
    ax.set_title(r'Optimal control')
    fig.colorbar(cax)
    # plot the optimal value function for each state
    fig, ax = plt.subplots()
    V = np.amax(q_table, axis=2)
    cax = ax.contourf(thX, thdotY, V, cmap=plt.cm.jet)
    ax.set_xlabel(r'$\theta$')
    ax.set_ylabel(r'$\omega$')
    ax.set_title(r'Optimal Q')
    fig.colorbar(cax)
    plt.show()


def select_action(ind0, explore_rate):
    # Select a random action
    if random.random() < explore_rate:
        actind = [np.random.randint(sz) for sz in NUM_ACTION_BUCKETS]
    # Select the action with the highest q
    else:
        if(DEBUG_MODE):
            table = q_table[ind0]
        actind = np.argmax(q_table[ind0], axis=-1)
        actind = [actind]
    return actind


def get_explore_rate(t):
    return max(MIN_EXPLORE_RATE, min(1.0, 1.0 - math.log10((float(t)+1)/25.0)))


def get_learning_rate(t):
    return 0.99
    # return max(MIN_LEARNING_RATE, min(0.5, 1.0 - math.log10((float(t)+1)/25.0)))


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-learn', '-l', action='store_true', default=False, help='learn it')
    parser.add_argument('-show', '-s', action='store_true', default=False, help='show it')
    parser.add_argument('-cont', '-c', action='store_true', default=False, help='continue')
    parser.add_argument('-plot', '-p', action='store_true', default=False, help='plot policy')
    args = parser.parse_args()
    main(args)
