#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
externalmodel.py

Define several user-defined models.
"""
import gym
from gym import error, spaces, utils
from gym.utils import seeding
from scipy.integrate import odeint
import numpy as np
import sys


class oneDBug(gym.Env):
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 30
    }

    def __init__(self):
        self.dt = 0.1
        self.viewer = None
        self.dimx, self.dimu = 2, 1
        self.xlb = np.array([-3, -5])
        self.xub = np.array([3, 5])
        self.action_space = spaces.Box(-0.5*np.ones(self.dimu), 0.5*np.ones(self.dimu))
        self.observation_space = spaces.Box(self.xlb, self.xub)

    def _get_obs(self):
        # return self.state.copy()
        return np.clip(self.state, self.xlb, self.xub)

    def getCost(self):
        obs = self._get_obs()
        cost = np.sum(obs**2)
        return cost

    def _step(self, a):
        self.state[0] += self.dt * self.state[1]
        self.state[1] += self.dt * a[0]
        return self._get_obs(), -self.getCost(), False, None

    def _reset(self):
        self.state = np.random.uniform([-1, -1], [1, 1])
        return self._get_obs()

    def _render(self, mode='human', close=False):
        if close:
            if self.viewer is not None:
                self.viewer.close()
                self.viewer = None
            return

        screen_width, screen_height = 500, 500
        scale = screen_width / (self.xub[0] - self.xlb[0])
        carlen, carwidth = 40.0/scale, 20.0/scale

        if self.viewer is None:
            from gym.envs.classic_control import rendering
            print('Create rendering env now')
            self.viewer = rendering.Viewer(screen_width, screen_height)
            self.viewer.set_bounds(5*self.xlb[0], 5*self.xub[0], 5*self.xlb[0], 5*self.xub[0])

            l, r, t, b = -carlen/2, carlen/2, carwidth/2, -carwidth/2
            car = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            self.cartrans = rendering.Transform()
            car.add_attr(self.cartrans)
            self.viewer.add_geom(car)

        x, v = self.state
        self.viewer.draw_line((0, -1), (0, 1))
        self.cartrans.set_translation(x, 0)
        sys.stdout.write('\rx {} v {}'.format(x, v))
        sys.stdout.flush()
        return self.viewer.render(return_rgb_array = mode=='rgb_array')


class DubinCarEnv(gym.Env):
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 30
    }

    def __init__(self, wrap=True):
        self.dt = 0.1
        self.viewer = None
        # define problem dimension
        self.dimx, self.dimu = 4, 2
        self.xlb = np.array([-2, -2, -np.pi, -5.1])
        self.xub = np.array([10, 10, np.pi, 5.1])
        # define costs
        self.Q = 0.1 * np.ones(self.dimx)
        self.Q[3] = 0.01
        self.Q[2] = 0.01
        self.R = 1.0 * np.ones(self.dimu)
        self.ulb = -5 * np.ones(self.dimu)
        self.uub = 5 * np.ones(self.dimu)
        self.tfweight = 1.0
        self.action_space = spaces.Box(self.ulb, self.uub)
        self.observation_space = spaces.Box(np.array([-100, -100, -1, -1, -10]),
                                            np.array([100, 100, 1, 1, 10]))
        self.default_state = np.array([5.0, 5.0, 0.0, 0.0])
        self.wrap = wrap
        # self._seed()
        self.viewer = None
        self.state = None

    def getCost(self, state, action):
        cpstate = state.copy()
        cpstate[2] = np.mod(state[2] + np.pi, 2*np.pi) - np.pi
        objQ = np.sum(cpstate**2 * self.Q)
        objR = np.sum(action**2 * self.R)
        fixcost = self.tfweight  # hope this encourage quick response
        return (objQ + objR + fixcost) * self.dt

    def _step(self, action):
        u = (self.uub - self.ulb)/2.0*action + (self.uub + self.ulb)/2.0
        u = np.clip(u, self.ulb, self.uub)
        y = odeint(self.dyn, self.state, np.array([0.0, self.dt]), args=(u,))
        costs = self.getCost(self.state, u)
        self.state = y[-1]
        x, y, theta, v = self.state
        finish = 0
        if np.abs(x) > 15 or np.abs(y) > 15 or np.abs(v) > 10 or np.abs(theta) > 4*np.pi:
            finish = 1
            reward = -1000.0
        elif np.linalg.norm(self.state) < 0.5:
            finish = 1
            reward = 100.0
        else:
            reward = 0
        return self._get_obs(), -costs, finish, reward

    def _get_obs(self):
        agl = self.state[2]
        outstate = np.clip(self.state, self.xlb, self.xub)
        outstate[2] = agl
        x, y, theta, v = outstate
        if self.wrap:
            return np.array([x, y, np.sin(theta), np.cos(theta), v])
        else:
            outtheta = np.mod(theta + np.pi, 2*np.pi) - np.pi
            return np.array([x, y, outtheta, v])

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _reset(self):
        # self.state = self.np_random.uniform(low=self.xlb, high=self.xub)
        self.state = np.random.uniform([5, 5, 0, -0.0], [10, 10, 0.0, 0.0])
        # self.state = self.default_state + np.random.normal(size=self.dimx)*0.01
        return self._get_obs()

    def _render(self, mode='human', close=False):
        if close:
            if self.viewer is not None:
                self.viewer.close()
                self.viewer = None
            return

        screen_width, screen_height = 500, 500
        scale = screen_width / (self.xub[0] - self.xlb[0])
        carlen, carwidth = 40/scale, 20/scale

        if self.viewer is None:
            from gym.envs.classic_control import rendering
            print('Create rendering env now')
            self.viewer = rendering.Viewer(screen_width, screen_height)
            self.viewer.set_bounds(self.xlb[0], self.xub[0], self.xlb[1], self.xub[1])

            l, r, t, b = -carlen/2, carlen/2, carwidth/2, -carwidth/2
            # car = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            car = rendering.FilledPolygon([(l, b), (l, t), (r, 0)])
            car.set_color(.8, .3, .3)
            self.cartrans = rendering.Transform()
            car.add_attr(self.cartrans)
            self.viewer.add_geom(car)
            # targetcar = rendering.FilledPolygon([(l, b), (l, t), (r, 0)])
            # targetcar.set_color(0, 1, 0)
            # defaultcartrans = rendering.Transform()
            # targetcar.add_attr(defaultcartrans)
            # self.viewer.add_geom(targetcar)

        x, y, theta, v = self.state
        self.cartrans.set_rotation(-theta + np.pi/2)
        self.viewer.draw_line((-1, 0), (1, 0))
        self.viewer.draw_line((0, -1), (0, 1))
        # self.cartrans.set_translation(x*scale + screen_width/2, y*scale + screen_height/2)
        # self.cartrans.set_translation(screen_width/scale/2. + x, screen_height/scale/2. + y)
        self.cartrans.set_translation(x, y)
        # self.cartrans.set_translation(30, 30)
        sys.stdout.write('\rx {} y {} theta {} v {}'.format(x, y, theta, v))
        sys.stdout.flush()

        return self.viewer.render(return_rgb_array = mode=='rgb_array')

    def dyn(self, x, t0, u):
        sta = np.sin(x[2])
        cta = np.cos(x[2])
        v = x[3]
        return np.array([v*sta, v*cta, u[0]*v, u[1]])


def main():
    import time
    env = DubinCarEnv()
    env.seed(13)
    env.reset()
    print(env.state)
    for _ in range(1):
        action = np.random.normal(size=2)
        env.step(action)
        env.render()
        print(env.state)
        time.sleep(0.05)
    # raw_input("Press Enter to continue")
    #env = gym.make('Acrobot-v1')
    #env.reset()
    #for _ in xrange(100):
    #    # action = np.random.normal(size=1)
    #    action = np.random.randint(3)
    #    env.step(action)
    #    env.render()
    #    time.sleep(0.1)


if __name__ == '__main__':
    main()
