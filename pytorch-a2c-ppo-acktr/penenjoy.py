#! /usr/bin/env python3
import argparse
import os, sys
import types

import numpy as np
import torch
from torch.autograd import Variable
import gym
# from externalmodel import pendulum
from knownPendulum import knownPendulum


parser = argparse.ArgumentParser(description='RL')
parser.add_argument('--seed', type=int, default=1,
                    help='random seed (default: 1)')
parser.add_argument('--num-stack', type=int, default=1,
                    help='number of frames to stack (default: 1)')
parser.add_argument('--log-interval', type=int, default=10,
                    help='log interval, one log per n updates (default: 10)')
parser.add_argument('--env-name', default='Pendulum-v0',
                    help='environment to train on (default: Pendulum-v0)')
parser.add_argument('--load-dir', default='./trained_models/ppo',
                    help='directory to save agent logs (default: ./trained_models/)')
args = parser.parse_args()


# env = gym.make(args.env_name)
env = pendulum()

actor_critic, allrewards, ob_rms = \
            torch.load(os.path.join(args.load_dir, args.env_name + ".pt"))


def normalize(obs):
    if obs.ndim == 2:
        obs = np.squeeze(obs, axis=1)
    newobs = torch.from_numpy((obs - ob_rms.mean) / np.sqrt(ob_rms.var)).float()
    return torch.unsqueeze(newobs, 0)


obs = env.reset()
obs0 = obs.copy()
print('Observation is {}'.format(obs))
# env.render('human')

states = torch.zeros(1, actor_critic.state_size)
masks = torch.zeros(1, 1)
step = 0
while True:
    current_obs = normalize(obs)
    value, action, _, states = actor_critic.act(Variable(current_obs, volatile=True),
                                                Variable(states, volatile=True),
                                                Variable(masks, volatile=True),
                                                deterministic=True)
    states = states.data
    cpu_actions = action.data.squeeze(1).cpu().numpy()
    # Obser reward and next obs
    if cpu_actions.ndim == 1:
        cpu_actions = np.expand_dims(cpu_actions, axis=0)
    obs, reward, done, _ = env.step(cpu_actions)
    step += 1
    # env.render('human')
    if done:
        print('State from {} end at step {} state {}'.format(obs0, step, obs))
        obs = env.reset()
        step = 0
