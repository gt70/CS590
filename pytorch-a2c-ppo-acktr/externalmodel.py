#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
externalmodel.py

Define several user-defined models.
"""
import gym
from gym import error, spaces, utils
from gym.utils import seeding
from scipy.integrate import odeint
import numpy as np
import sys


class oneDBug(gym.Env):
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 30
    }

    def __init__(self):
        self.dt = 0.1
        self.viewer = None
        self.dimx, self.dimu = 2, 1
        self.xlb = np.array([-3, -5])
        self.xub = np.array([3, 5])
        self.action_space = spaces.Box(-0.5*np.ones(self.dimu), 0.5*np.ones(self.dimu))
        self.observation_space = spaces.Box(self.xlb, self.xub)
        self.reset()

    def _get_obs(self):
        # return self.state.copy()
        return np.clip(self.state, self.xlb, self.xub)

    def getCost(self):
        obs = self._get_obs()
        cost = np.sum(obs**2)
        return cost

    def _step(self, a):
        self.state[0] += self.dt * self.state[1]
        self.state[1] += self.dt * a[0]
        if np.linalg.norm(self.state) < 0.1:
            return self._get_obs(), 100, True, {}
        elif np.linalg.norm(self.state) > 5:
            return self._get_obs(), -100, True, {}
        else:
            return self._get_obs(), -self.getCost(), False, {}

    def _reset(self):
        self.state = np.random.uniform([-1, -1], [1, 1])
        return self._get_obs()

    def _render(self, mode='human', close=False):
        if close:
            if self.viewer is not None:
                self.viewer.close()
                self.viewer = None
            return

        screen_width, screen_height = 500, 500
        scale = screen_width / (self.xub[0] - self.xlb[0])
        carlen, carwidth = 40.0/scale, 20.0/scale

        if self.viewer is None:
            from gym.envs.classic_control import rendering
            print('Create rendering env now')
            self.viewer = rendering.Viewer(screen_width, screen_height)
            self.viewer.set_bounds(5*self.xlb[0], 5*self.xub[0], 5*self.xlb[0], 5*self.xub[0])

            l, r, t, b = -carlen/2, carlen/2, carwidth/2, -carwidth/2
            car = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            self.cartrans = rendering.Transform()
            car.add_attr(self.cartrans)
            self.viewer.add_geom(car)

        x, v = self.state
        self.viewer.draw_line((0, -1), (0, 1))
        self.cartrans.set_translation(x, 0)
        sys.stdout.write('\rx {} v {}'.format(x, v))
        sys.stdout.flush()
        return self.viewer.render(return_rgb_array = mode=='rgb_array')


class DubinCarEnv(gym.Env):
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 30
    }

    def __init__(self):
        self.dt = 0.1
        self.maxtime = 15.0
        self.viewer = None
        # define problem dimension
        self.dimx, self.dimu = 4, 2
        self.xlb = np.array([-10, -10, -np.pi, -3.1])
        self.xub = np.array([10, 10, np.pi, 3.1])
        self.ulb = -5*np.ones(self.dimu)
        self.uub = -self.ulb
        # define costs
        self.Q = 1*np.ones(self.dimx)
        self.Q[2] = self.Q[3] = 0.01
        self.R = np.ones(self.dimu)
        self.tfweight = 10
        self.action_space = spaces.Box(-np.ones_like(self.ulb), np.ones_like(self.uub))
        self.observation_space = spaces.Box(np.array([-100, -100, -1, -1, -10]),
                                            np.array([100, 100, 1, 1, 10]))
        # self._seed()
        self.viewer = None
        self.state = None

    def getCost(self, state, action):
        th = np.mod(state[2] + np.pi, 2*np.pi) - np.pi
        cpstate = state.copy()
        cpstate[2] = th
        objQ = np.sum(cpstate**2 * self.Q)
        objR = np.sum(action**2 * self.R)
        fixcost = self.tfweight
        return (objQ + objR + fixcost) * self.dt * 0.01

    def _step(self, action):
        u = action * (self.uub - self.ulb) / 2. + (self.uub + self.ulb) / 2
        y = odeint(self.dyn, self.state, np.array([0.0, self.dt]), args=(u,))
        costs = self.getCost(self.state, u)
        self.state = y[-1]
        x, y, theta, v = self.state
        finish = False
        if np.abs(x) > 10 or np.abs(y) > 10 or np.abs(v) > 10 or np.abs(theta) > 4*np.pi:
            finish = True
            costs += 20
        else:
            statenorm = np.linalg.norm(self.state)
            if statenorm < 1.0:
                costs += -20*(1.0 - statenorm) * self.dt
            if statenorm < 0.5:
                finish = True
        self.time += self.dt
        if self.time >= self.maxtime:
            finish = True
        return self._get_obs(), -costs, finish, {}

    def _get_obs(self):
        x, y, theta, v = self.state
        obs = np.array([x, y, np.sin(theta), np.cos(theta), v])
        # return np.clip(obs, self.xlb, self.xub)
        return obs

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _reset(self):
        # self.state = self.np_random.uniform(low=self.xlb, high=self.xub)
        self.state = self.np_random.uniform([-0.5, 3, -0.3, -0.5], [0.5, 5, 0.3, 0.5])
        self.time = 0.0
        return self._get_obs()

    def _render(self, mode='human', close=False):
        if close:
            if self.viewer is not None:
                self.viewer.close()
                self.viewer = None
            return

        screen_width, screen_height = 500, 500
        scale = screen_width / (self.xub[0] - self.xlb[0])
        carlen, carwidth = 40/scale, 20/scale

        if self.viewer is None:
            from gym.envs.classic_control import rendering
            # print('Create rendering env now')
            self.viewer = rendering.Viewer(screen_width, screen_height)
            self.viewer.set_bounds(self.xlb[0], self.xub[0], self.xlb[1], self.xub[1])

            l, r, t, b = -carlen/2, carlen/2, carwidth/2, -carwidth/2
            # car = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            car = rendering.FilledPolygon([(l, b), (l, t), (r, 0)])
            car.set_color(.8, .3, .3)
            self.cartrans = rendering.Transform()
            car.add_attr(self.cartrans)
            self.viewer.add_geom(car)

        x, y, theta, v = self.state
        self.cartrans.set_rotation(-theta + np.pi/2)
        self.viewer.draw_line((-1, 0), (1, 0))
        self.viewer.draw_line((0, -1), (0, 1))
        # self.cartrans.set_translation(x*scale + screen_width/2, y*scale + screen_height/2)
        # self.cartrans.set_translation(screen_width/scale/2. + x, screen_height/scale/2. + y)
        self.cartrans.set_translation(x, y)
        # self.cartrans.set_translation(30, 30)
        # sys.stdout.write('\rx {} y {} theta {} v {}'.format(x, y, theta, v))
        # sys.stdout.flush()

        return self.viewer.render(return_rgb_array = mode=='rgb_array')

    def dyn(self, x, t0, u):
        sta = np.sin(x[2])
        cta = np.cos(x[2])
        v = x[3]
        return np.array([v*sta, v*cta, u[0]*v, u[1]])


class QuadCopEnv(gym.Env):
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 30
    }

    def __init__(self):
        self.dt = 0.1
        self.maxtime = 15.0
        self.viewer = None
        self.max_omega = 3.0
        self.max_u = 1.0
        # define problem dimension
        self.dimx, self.dimu = 6, 2  # x, y, vx, vy, theta, omega
        self.xlb = np.array([-5, -5, -10, -10, -np.pi, -self.max_omega])
        self.xub = np.array([5, 5, 10, 10, np.pi, self.max_omega])
        self.x0lb = np.array([-1, 4, 0, 0, 0, 0])
        self.x0ub = np.array([1, 5, 0, 0, 0, 0])
        self.ulb = np.zeros(self.dimu)
        self.uub = self.max_u * np.ones(self.dimu)
        # define costs
        self.Q = 0.0*np.ones(self.dimx)
        self.Q[0] = self.Q[1] = 1.0
        self.Q[2] = self.Q[3] = 0.1
        self.Q[4] = self.Q[5] = 0.5  # penalize more
        self.R = 0.01 * np.ones(self.dimu)  # maybe not needed
        self.action_space = spaces.Box(-np.ones_like(self.ulb), np.ones_like(self.uub))
        self.observation_space = spaces.Box(np.array([-10, -10, -10, -10, -1, -1, -self.max_omega]),
                                            np.array([10, 10, 10, 10, 1, 1, self.max_omega]))
        self.state = None
        self.encourage = True
        self.discourage = True

    def getCost(self, state, action):
        theta0 = state[4]
        th = np.mod(state[4] + np.pi, 2*np.pi) - np.pi  # within -pi, pi
        state[4] = th
        objQ = np.sum(np.abs(state) * self.Q)  # to avoid position gets too large penalty
        objR = np.sum(action**2 * self.R)
        state[4] = theta0
        return (objQ + objR) * self.dt

    def getreward(self, state):
        theta0 = state[4]
        state[4] = np.mod(state[4] + np.pi, 2*np.pi) - np.pi
        normState = np.linalg.norm(state)
        finish = False
        reward = 0.0
        if normState < 0.5:
            if self.encourage:
                reward += 20.0
            finish = True
        if normState > 15.0:
            if self.discourage:
                reward = -50  # this needs tuning
            finish = True
        state[4] = theta0
        return reward, finish

    def _step(self, action):
        u = action * (self.uub - self.ulb) / 2. + (self.uub + self.ulb) / 2
        df = self.dyn(self.state, u)
        self.state += df * self.dt  # first order Euler
        costs = self.getCost(self.state, u)
        reward, finish = self.getreward(self.state)
        self.time += self.dt
        if self.time >= self.maxtime:
            finish = True
        return self._get_obs(), -costs + reward, finish, {}

    def _get_obs(self):
        x, y, vx, vy, theta, omega = self.state
        obs = np.array([x, y, vx, vy, np.sin(theta), np.cos(theta), omega])
        # return np.clip(obs, self.xlb, self.xub)
        return obs

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _reset(self):
        # self.state = self.np_random.uniform(low=self.xlb, high=self.xub)
        self.state = self.np_random.uniform(self.x0lb, self.x0ub)
        self.time = 0.0
        return self._get_obs()

    def _render(self, mode='human', close=False):
        if close:
            if self.viewer is not None:
                self.viewer.close()
                self.viewer = None
            return

        screen_width, screen_height = 500, 500
        scale = screen_width / (self.xub[0] - self.xlb[0])
        carlen, carwidth = 40/scale, 10/scale

        if self.viewer is None:
            from gym.envs.classic_control import rendering
            print('Create rendering env now')
            self.viewer = rendering.Viewer(screen_width, screen_height)
            self.viewer.set_bounds(self.xlb[0], self.xub[0], self.xlb[1], self.xub[1])

            l, r, t, b = -carlen/2, carlen/2, carwidth/2, -carwidth/2
            # car = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            car = rendering.FilledPolygon([(l, 0), (0, t), (r, 0)])
            car.set_color(.8, .3, .3)
            self.cartrans = rendering.Transform()
            car.add_attr(self.cartrans)
            self.viewer.add_geom(car)

        x, y, theta = self.state[0], self.state[1], self.state[4]
        self.cartrans.set_rotation(theta)
        self.viewer.draw_line((-1, 0), (1, 0))
        self.viewer.draw_line((0, -1), (0, 1))
        self.cartrans.set_translation(x, y)
        # self.cartrans.set_translation(30, 30)
        sys.stdout.write('\rx {} y {} theta {}'.format(x, y, theta))
        sys.stdout.flush()
        return self.viewer.render(return_rgb_array = mode=='rgb_array')

    def dyn(self, x, u):
        k1 = 1.0  # for acceleration
        k2 = 1.0  # for angular acceleration
        g = 1.0
        x, y, vx, vy, theta, omega = x
        u1, u2 = u
        sumu = u1 + u2
        sth = np.sin(theta)
        cth = np.cos(theta)
        f = np.array([vx, vy, -sumu*sth, -g+sumu*cth, omega, (u2 - u1)*k2])
        return f


class pendulum(gym.Env):
    metadata = {
        'render.modes' : ['human', 'rgb_array'],
        'video.frames_per_second' : 30
    }

    def __init__(self):
        self.max_speed=8
        self.max_torque=2.
        self.dt=.05
        self.viewer = None

        high = np.array([1., 1., self.max_speed])
        self.action_space = spaces.Box(low=-self.max_torque, high=self.max_torque, shape=(1,))
        self.observation_space = spaces.Box(low=-high, high=high)

        self._seed()

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _step(self,u):
        th, thdot = self.state # th := theta

        g = 10.
        m = 1.
        l = 1.
        dt = self.dt

        u = np.clip(u, -self.max_torque, self.max_torque)[0]
        self.last_u = u # for rendering
        costs = angle_normalize(th)**2 + .1*thdot**2 + .001*(u**2)
        done = False

        newthdot = thdot + (-3*g/(2*l) * np.sin(th + np.pi) + 3./(m*l**2)*u) * dt
        newth = th + newthdot*dt
        if np.abs(newth) > self.max_speed:
            done = True
            costs += 100
        if np.abs(angle_normalize(newth)) < 0.2 and np.abs(newthdot) < 0.2:
            done = True
            # print('We reached goal once')
            costs -= 100
        newthdot = np.clip(newthdot, -self.max_speed, self.max_speed) #pylint: disable=E1111

        self.state = np.array([newth, newthdot])

        return self._get_obs(), -costs, done, {}

    def _reset(self):
        high = np.array([np.pi, 1])
        self.state = self.np_random.uniform(low=-high, high=high)
        self.last_u = None
        return self._get_obs()

    def _get_obs(self):
        theta, thetadot = self.state
        return np.array([np.cos(theta), np.sin(theta), thetadot])

    def _render(self, mode='human', close=False):
        if close:
            if self.viewer is not None:
                self.viewer.close()
                self.viewer = None
            return

        if self.viewer is None:
            from gym.envs.classic_control import rendering
            self.viewer = rendering.Viewer(500,500)
            self.viewer.set_bounds(-2.2,2.2,-2.2,2.2)
            rod = rendering.make_capsule(1, .2)
            rod.set_color(.8, .3, .3)
            self.pole_transform = rendering.Transform()
            rod.add_attr(self.pole_transform)
            self.viewer.add_geom(rod)
            axle = rendering.make_circle(.05)
            axle.set_color(0,0,0)
            self.viewer.add_geom(axle)
            fname = path.join(path.dirname(__file__), "assets/clockwise.png")
            self.img = rendering.Image(fname, 1., 1.)
            self.imgtrans = rendering.Transform()
            self.img.add_attr(self.imgtrans)

        self.viewer.add_onetime(self.img)
        self.pole_transform.set_rotation(self.state[0] + np.pi/2)
        if self.last_u:
            self.imgtrans.scale = (-self.last_u/2, np.abs(self.last_u)/2)

        return self.viewer.render(return_rgb_array = mode=='rgb_array')

def angle_normalize(x):
    return (((x+np.pi) % (2*np.pi)) - np.pi)

def main():
    import time
    env = DubinCarEnv()
    env.seed(13)
    env.reset()
    print(env.state)
    for _ in range(1):
        action = np.random.normal(size=2)
        env.step(action)
        env.render()
        print(env.state)
        time.sleep(0.05)
    # raw_input("Press Enter to continue")
    #env = gym.make('Acrobot-v1')
    #env.reset()
    #for _ in xrange(100):
    #    # action = np.random.normal(size=1)
    #    action = np.random.randint(3)
    #    env.step(action)
    #    env.render()
    #    time.sleep(0.1)


if __name__ == '__main__':
    main()
