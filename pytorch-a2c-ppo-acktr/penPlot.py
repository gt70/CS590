#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
penPlot.py

Afraid neural network cannot learn a policy good enough for my case, check the policy and value network.
I decide to extract his model and solve this problem using traditional techniques, so I can directly compare.
"""
import numpy as np
import torch
import os
from torch.autograd import Variable
import gym
import matplotlib.pyplot as plt
from externalmodel import pendulum


env_name = 'Pendulum-v0'
load_dir = './trained_models/ppo'
# env = gym.make(env_name)
env = pendulum()

actor_critic, allrewards, ob_rms = \
            torch.load(os.path.join(load_dir, env_name + ".pt"))
for rwd in allrewards:
    plt.plot(rwd)
    plt.show()


def normalize(obs):
    if obs.ndim == 2:
        if obs.shape[1] == 1:
            obs = np.squeeze(obs, axis=1)
    newobs = torch.from_numpy((obs - ob_rms.mean) / np.sqrt(ob_rms.var)).float()
    if newobs.dim() == 1:
        return torch.unsqueeze(newobs, 0)
    else:
        return newobs


# create grid of the states
vtheta = np.linspace(-np.pi, np.pi, 50)
vOmega = np.linspace(-8, 8, 100)  # yes, 8 is quite high
mTh, mOmg = np.meshgrid(vtheta, vOmega, indexing='ij')
nTh, nOmg = len(vtheta), len(vOmega)
mValue, mPolicy = np.zeros_like(mTh), np.zeros_like(mTh)
states = torch.zeros(nOmg, actor_critic.state_size)
masks = torch.zeros(nOmg, 1)
# loop over angle, and Omega, respectively
for i, theta in enumerate(vtheta):
    cth, sth = np.cos(theta), np.sin(theta)
    obs = np.concatenate((cth*np.ones((nOmg, 1)), sth*np.ones((nOmg, 1)), vOmega[:, np.newaxis]), axis=1)
    current_obs = normalize(obs)
    value, action, _, states_ = actor_critic.act(Variable(current_obs, volatile=True),
                                                Variable(states, volatile=True),
                                                Variable(masks, volatile=True),
                                                deterministic=True)
    cpuvalue = np.squeeze(value.data.cpu().numpy(), axis=1)
    cpuaction = np.squeeze(action.data.cpu().numpy(), axis=1)
    # Try to insert into those two matrices
    mValue[i, :] = cpuvalue
    mPolicy[i, :] = cpuaction
# draw two figures
fig, ax = plt.subplots()
cax = ax.contourf(mTh, mOmg, mValue, cmap=plt.cm.jet)
fig.colorbar(cax)
ax.set_title('value')
ax.set_xlabel(r'$\theta$')
ax.set_ylabel(r'$\omega$')
fig.savefig('pendulum_value.pdf')
fig, ax = plt.subplots()
cax = ax.contourf(mTh, mOmg, mPolicy, cmap=plt.cm.jet)
ax.set_title('policy')
ax.set_xlabel(r'$\theta$')
ax.set_ylabel(r'$\omega$')
fig.colorbar(cax)
fig.savefig('pendulum_policy.pdf')
plt.show()
