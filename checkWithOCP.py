#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
checkWithOCP.py

Check if the OCP results outperforms qlearning / policy gradient
"""
import numpy as np
import matplotlib.pyplot as plt
import json
import sys
sys.path.append('/home/motion')
from GAO.ExplicitMPC.pendulum.Script.utility import glbVar, getConfig, parseX, addPlot, bvpSolve
from qPen import oneSim


cfg = getConfig(glbVar.EXPCFGFILE)
N, dimx, dimu = cfg['N'], cfg['dimx'], cfg['dimu']


def main():
    # plotCmp()
    massiveCompare()


def massiveCompare():
    """Select some initial states within some bound, check two algorithms"""
    xlb = np.array([0, -2.0])
    xub = np.array([np.pi, 2.0])
    simN = 50
    for sim in range(simN):
        fig, ax = plt.subplots()
        x0 = xlb + (xub - xlb) * np.random.uniform(size=xlb.shape)
        simRst = oneSim(x0)
        addPlot(ax, simRst['vX'], simRst['vU'], simRst['vt'], cfg)
        # add another one by solving
        sol = solveBest(x0)
        if sol is not None:
            addPlot(ax, sol['vX'], sol['vU'], sol['vt'], cfg)
        plt.show()


def solveBest(x0):
    """enumerate three targets to find the optimal"""
    cfg['x0'] = x0.tolist()
    cfg['tf'][1] = 10.0
    vxf = [[-np.pi, 0], [np.pi, 0], [3*np.pi, 0]]
    bestJ = 1e10
    for xf in vxf:
        cfg['xf'] = xf
        sol = bvpSolve(cfg, False, True, True)
        if sol['flag'] == 1:
            if sol['obj'] < bestJ:
                bestJ = sol['obj']
                bestX = np.array(sol['x'])
    if bestJ != 1e10:
        tjX, tjU, tf = parseX(bestX, N, dimx, dimu)
        return {'J': bestJ, 'vX': tjX, 'vU': tjU, 'vt': tf}
    else:
        return None



def plotCmp():
    ocpSol = readOCPsol()
    tjX, tjU, tf = parseX(ocpSol['x'], N, dimx, dimu)
    x0 = tjX[0]
    print('We start from {}'.format(x0))
    qSol = getqSol(x0)
    # maybe we directly plot one traj now
    fig, ax = plt.subplots()
    addPlot(ax, x=tjX, u=tjU, t=tf, cfg=cfg)
    # add another one
    addPlot(ax, qSol['vX'], qSol['vU'], qSol['vt'], cfg)
    plt.show()


def getqSol(x0):
    return oneSim(x0, False)


def readOCPsol():
    """Read the json file to learn something"""
    filePath = '/home/motion/GAO/ExplicitMPC/pendulum/Result/tmpout.json'
    sol = getConfig(filePath)
    return sol


if __name__ == '__main__':
    main()
