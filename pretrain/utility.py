#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
utility.py

Utility function for pretrained model. 
It might be bad, but how to do this? 
"""
import os, sys, time
import numpy as np
import torch
from torch.autograd import Variable
import pickle as pkl
from torchUtil import GaoNet
#from GAO.ExplicitMPC.pendulum.Script import torchUtil
#from GAO.ExplicitMPC.pendulum.Script.torchUtil import modelLoaderV2, GaoNet
#from GAO.ExplicitMPC.pendulum.Script.utility import glbVar


def loadModel(fnm, cudafy=False):
    #mdl, xScale, yScale = modelLoaderV2(mdlName, cudafy)
    mdl, xScale, yScale = torch.load(fnm)
    if cudafy:
        mdl.cuda()
    """
    def x2y(x):  # evaluate the model, np 2 np
        if x.ndim == 1:
            xin = x[np.newaxis, :]
        else:
            xin = x
        if xScale is not None:
            xin = (xin - xScale[0]) / xScale[1]
        fdx = Variable(torch.from_numpy(xin).float(), requires_grad=False)
        if cudafy:
            fdx.cuda()
        pdy = mdl(fdx).cpu().data.numpy()
        if yScale is not None:
            pdy = pdy * yScale[1] + yScale[0]
        if x.ndim == 1:
            pdy = np.squeeze(pdy, axis=0)
        return pdy
    """
    return {'model': mdl, 'xScale': xScale, 'yScale': yScale}
