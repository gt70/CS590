#! /usr/bin/env python3
import argparse
import os, sys
import types

import numpy as np
import matplotlib.pyplot as plt
import torch
from torch.autograd import Variable
import gym
PARDIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(PARDIR)
from sppo import sPPO
from model import knownPendulum
from utility import loadModel


parser = argparse.ArgumentParser(description='RL')
parser.add_argument('--seed', type=int, default=1,
                    help='random seed (default: 1)')
parser.add_argument('--num-stack', type=int, default=1,
                    help='number of frames to stack (default: 1)')
parser.add_argument('--log-interval', type=int, default=10,
                    help='log interval, one log per n updates (default: 10)')
parser.add_argument('--env-name', default='knownPendulum',
                    help='environment to train on (default: knownPendulum)')
parser.add_argument('--load-dir', default='./trained_models/ppo',
                    help='directory to save agent logs (default: ./trained_models/ppo)')
parser.add_argument('--model0', action='store_true', default=False, help='See how the model before training works')
parser.add_argument('--sim', action='store_true', default=False, help='enable so we simulate the system')
parser.add_argument('--plot', action='store_true', default=False, help='enable so we plot the fields')
parser.add_argument('--simNum', type=int, default=100, help='how many simulations we perform')
args = parser.parse_args()


env = knownPendulum()
pklName = 'sppoPenPretrain.pkl'


def main():
    ppo = getModel()
    if args.plot:
        plotField(ppo)
    if args.sim:
        vx0, vxf = simulate(ppo, args.simNum)
        vxf[:, 0] = np.mod(vxf[:, 0], 2*np.pi) - np.pi
        normxf = np.linalg.norm(vxf, axis=1)
        print('histogram of xf is ', np.histogram(normxf))


def getModel():
    if not args.model0:
        # saved = torch.load(os.path.join(args.load_dir, args.env_name + ".pt"))
        saved = torch.load(pklName)
        ppo, final_rewards = saved[0], saved[1]
    else:
        valueName = 'value_2_100_1.pt'
        policyName = 'firstpolicy_2_50_1.pt'
        vMdl = loadModel(valueName)
        piMdl = loadModel(policyName)
        ppo = sPPO(env)
        ppo.addActNet(piMdl)
        ppo.addVNet(vMdl)
        ppo.valueNet.multiplier = -1.0
        ppo.actNet.cpu()
        ppo.valueNet.cpu()
    return ppo


def simulate(ppo, simN=100):
    obs = env.reset()
    obs0 = obs.copy()
    print('Observation is {}'.format(obs))
    # env.render('human')
    donenum = 0
    vX0 = np.zeros((simN, len(obs)))
    vXf = np.zeros((simN, len(obs)))

    states = torch.zeros(1, 2)
    masks = torch.zeros(1, 1)
    step = 0
    print(obs0)
    vX0[donenum] = obs0
    while True:
        value, action, action_log_prob, states = ppo.act(Variable(torch.from_numpy(obs).float(), volatile=True),
                                                                  Variable(torch.Tensor([0]), volatile=True),
                                                                  Variable(torch.Tensor([0]), volatile=True),
                                                                  deterministic=True)
        cpu_actions = action.data.squeeze(1).cpu().numpy()
        # Obser reward and next obs
        # if cpu_actions.ndim == 1:
        #    cpu_actions = np.expand_dims(cpu_actions, axis=0)
        obs, reward, done, _ = env.step(cpu_actions)
        step += 1
        # env.render('human')
        if done:
            print('State from {} end at step {} state {}'.format(obs0, step, obs))
            vXf[donenum] = obs
            donenum += 1
            if donenum >= simN:
                return vX0, vXf
            obs = env.reset()
            obs0 = obs.copy()
            step = 0
            vX0[donenum] = obs


def plotField(ppo):
    """Load the trained model and plot the field information"""
    # create a grid of theta and vel
    nAgl, nVel = 100, 200
    vecAngle = np.linspace(0, 2*np.pi, nAgl)
    vecVel = np.linspace(-2, 2, nVel)
    gridx, gridy = np.meshgrid(vecAngle, vecVel, indexing='ij')
    mValue = np.zeros_like(gridx)
    mPolicy = np.zeros_like(gridx)
    for i, agl in enumerate(vecAngle):
        sini, cosi = np.sin(agl)*np.ones(nVel), np.cos(agl)*np.ones(nVel)
        vagl = agl * np.ones(nVel)
        usex = np.stack([vagl, vecVel], axis=1)
        value, act = ppo.getVA(usex)
        act[act > 1] = 1.0
        act[act < -1.0] = -1.0
        mValue[i, :] = value.squeeze(axis=1)
        mPolicy[i, :] = act.squeeze(axis=1)
    # plot two figures
    fig, axs = plt.subplots()
    cax0 = axs.contourf(gridx, gridy, mValue, cmap=plt.cm.jet)
    axs.set_xlabel(r'$\theta$')
    axs.set_ylabel(r'$\omega$')
    axs.set_title(r'value function')
    fig.colorbar(cax0)
    fig, axs = plt.subplots()
    cax1 = axs.contourf(gridx, gridy, mPolicy, cmap=plt.cm.jet)
    axs.set_xlabel(r'$\theta$')
    axs.set_ylabel(r'$\omega$')
    axs.set_title(r'policy')
    fig.colorbar(cax1)
    plt.show()


if __name__ == '__main__':
    main()
