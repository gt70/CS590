#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
sppopen.py

See if we can use pretrained model and use RL to refine the model

I decide to copy what people have done, i.e. a storage class which stores many rollout results.
I will let ppo contain methods from MLPPolicy
"""
import os, sys, time
import torch
from torch.autograd import Variable
import gym
import numpy as np
import matplotlib.pyplot as plt
import pickle as pkl
PARDIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(PARDIR)
from sppo import sPPO
from model import knownPendulum
from baselines.common.vec_env.dummy_vec_env import DummyVecEnv
from baselines.common.vec_env.subproc_vec_env import SubprocVecEnv
from baselines.common.vec_env.vec_normalize import VecNormalize
sys.path.append(os.path.join(PARDIR, 'pytorch-a2c-ppo-acktr'))
from envs import make_env
from storage import RolloutStorage
from utility import loadModel

if False:
    import pydevd
    pydevd.settrace('10.197.7.254', port=10000, stdoutToServer=True, stderrToServer=True)

DEBUG = 0
pklName = 'sppoPenPretrain.pkl'

args_seed = 0
args_log_dir = './log'
args_num_steps = 1024
args_cuda = False
args_use_gae = False
args_gamma = 1.0
args_tau = 0.95
args_ppo_epoch = 4
args_num_mini_batch = 32
args_save_interval = 50
args_log_interval = args_save_interval
args_save_dir = 'trained_models'
args_num_processes = 1
args_env_name = 'knownPendulum'
maxIter = 300


def main(cont):
    environment = knownPendulum
    envs = [make_env(environment, args_seed, i, args_log_dir) for i in range(1)]
    envs = DummyVecEnv(envs)
    envs.envs[0].env.encourage = False
    envs.envs[0].env.discourage = False
    env = envs.envs[0].env  # token
    envs = VecNormalize(envs, False, False, -np.inf, np.inf, 1.0, 1e-8)  # not sure what eps does
    obs_shape = envs.observation_space.shape
    obs_shape = (obs_shape[0], *obs_shape[1:])
    rollouts = RolloutStorage(args_num_steps, 1, obs_shape, envs.action_space, 1)
    current_obs = torch.zeros(args_num_processes, *obs_shape)
    # build the network to be trained
    if not cont:
        # load two models
        valueName = 'value_2_100_1.pt'
        policyName = 'firstpolicy_2_50_1.pt'
        vMdl = loadModel(valueName)
        piMdl = loadModel(policyName)
        ppo = sPPO(env)
        ppo.addActNet(piMdl)
        ppo.addVNet(vMdl)
        ppo.valueNet.multiplier = -1.0
        if not args_cuda:
            ppo.actNet.cpu()
            ppo.oldActNet.cpu()
            ppo.valueNet.cpu()
    else:
        ppo = torch.load(pklName)[0]

    def update_current_obs(obs):
        shape_dim0 = envs.observation_space.shape[0]
        obs = torch.from_numpy(obs).float()
        current_obs[:, -shape_dim0:] = obs

    obs = envs.reset()
    update_current_obs(obs)

    rollouts.observations[0].copy_(current_obs)

    # These variables are used to compute average rewards for all processes.
    episode_rewards = torch.zeros([args_num_processes, 1])
    final_rewards = torch.zeros([args_num_processes, 1])
    record_final_rewards = [[] for _ in range(args_num_processes)]

    if args_cuda:
        current_obs = current_obs.cuda()
        rollouts.cuda()
    # setting for iteration process
    # start iteration
    start = time.time()
    for j in range(maxIter):
        for step in range(args_num_steps):  # collect enough data
            # Sample actions
            value, action, action_log_prob, states = ppo.act(Variable(rollouts.observations[step], volatile=True),
                                                                      Variable(rollouts.states[step], volatile=True),
                                                                      Variable(rollouts.masks[step], volatile=True))
            cpu_actions = action.data.squeeze(1).cpu().numpy()
            if cpu_actions.ndim == 1:
                cpu_actions = np.expand_dims(cpu_actions, axis=0)

            # Obser reward and next obs
            obs, reward, done, info = envs.step(cpu_actions)
            reward = torch.from_numpy(np.expand_dims(np.stack(reward), 1)).float()
            episode_rewards += reward

            # If done then clean the history of observations.
            masks = torch.FloatTensor([[0.0] if done_ else [1.0] for done_ in done])
            final_rewards *= masks
            final_rewards += (1 - masks) * episode_rewards
            episode_rewards *= masks
            for i, frwd in enumerate(final_rewards):
                if done[i]:
                    record_final_rewards[i].append(frwd[0])

            if args_cuda:
                masks = masks.cuda()

            if current_obs.dim() == 4:
                current_obs *= masks.unsqueeze(2).unsqueeze(2)
            else:
                current_obs *= masks

            update_current_obs(obs)
            rollouts.insert(step, current_obs, states.data, action.data, action_log_prob.data, value.data, reward, masks)
        # print latest 10 % of rewards, its mean, min, max, etc
        for rec in record_final_rewards:
            lenrec = len(rec)
            if lenrec > 0:
                recdt = np.array(rec[int(0.9*lenrec):lenrec])
                rmean, rstd, rmin, rmax = recdt.mean(), recdt.std(), recdt.min(), recdt.max()
                sys.stdout.write('\rAt Iter {}, mean/std {}, min/max/median {}'.format(j, [rmean, rstd], [rmin, rmax]))
                sys.stdout.flush()
        # calculate value function at next step using the observation
        next_value = ppo.getVA(Variable(rollouts.observations[-1], volatile=True))[0].data

        rollouts.compute_returns(next_value, args_use_gae, args_gamma, args_tau)  # calculate returns

        # extract data and do iterations
        diffRet = rollouts.returns[:-1] - rollouts.value_preds[:-1]
        for e in range(args_ppo_epoch):
            data_generator = rollouts.feed_forward_generator(diffRet, args_num_mini_batch)
            ppo.batch_update(data_generator)
        # prepare for next step
        rollouts.after_update()
        if (j + 1) % args_save_interval == 0 and args_save_dir != "":
            save_path = os.path.join(args_save_dir, 'ppo')
            try:
                os.makedirs(save_path)
            except OSError:
                pass

            # A really ugly way to save a model to CPU
            save_model = ppo
            if args_cuda:
                save_model = copy.deepcopy(ppo).cpu()

            save_model = [save_model, record_final_rewards, hasattr(envs, 'ob_rms') and envs.ob_rms or None]

            # torch.save(save_model, os.path.join(save_path, args_env_name + ".pt"))
            torch.save(save_model, pklName)

        if (j + 1) % args_log_interval == 0:
            end = time.time()
            total_num_steps = (j + 1) * args_num_processes * args_num_steps
            print("Updates {}, num timesteps {}, FPS {}, mean/median reward {:.1f}/{:.1f}, min/max reward {:.1f}/{:.1f}".
                format(j, total_num_steps,
                       int(total_num_steps / (end - start)),
                       final_rewards.mean(),
                       final_rewards.median(),
                       final_rewards.min(),
                       final_rewards.max()))
    # after training plot reward history
    # fig, ax = plt.subplots()
    # for rst in record_final_rewards:
    #     ax.plot(rst)
    # plt.show()


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', action='store_true', default=False)
    parser.add_argument('-cont', '-c', action='store_true', default=False)
    parser.add_argument('-plot', '-p', action='store_true', default=False)
    args = parser.parse_args()
    main(args.cont)
