#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
quadPretrain.py

Quadcopter problem, use pretrained database to get a better overview
"""
import os, sys, time
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F
import torch.optim as optim
import gym
import numpy as np
import matplotlib.pyplot as plt
import pickle as pkl
import copy
PARDIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
from baselines.common.vec_env.dummy_vec_env import DummyVecEnv
from baselines.common.vec_env.subproc_vec_env import SubprocVecEnv
from baselines.common.vec_env.vec_normalize import VecNormalize
sys.path.append(os.path.join(PARDIR, 'pytorch-a2c-ppo-acktr'))
from externalmodel import QuadCopEnv  # not sure which one will be imported
from envs import make_env
from storage import RolloutStorage
from control import lqr
sys.path.append(PARDIR)
from sppo import sPPO


# global settings such as neural network structure
aLyr = 50
vLyr = 50
env = QuadCopEnv
preMdlName = 'sppoPreQuad_pre.pt'
mdlName = 'sppoPreQuad.pt'


def main(args):
    """Main function."""
    if args.supervise:
        supervisedTrain(args)
    if args.learn:
        rlTrain(args)


def rlTrain(args):
    """Copy code from pendulum and start a fresh rl training, might be bad"""
    if args.scratch:
        ppo = sPPO(environ)
        ppo.addActNet([aLyr])
        ppo.addVNet([vLyr])
    elif not args.cont:
        ppo = loadModel(preMdlName)
        if isinstance(ppo, list):
            ppo = ppo[0]
        ppo.valueNet.multiplier = -1  # make it negative
        ppo.valueNet.offset = 20.0  # since we are encouraging
        # randomly assign weights to actNet
        for m in ppo.actNet.modules():
            if isinstance(m, nn.Linear):
                m.bias.data.normal_(0, 0.1)
                m.weight.data.normal_(0, 0.1)
        ppo.copyNet()
    else:
        ppo = loadModel(mdlName)
    args_seed = 0
    args_log_dir = './log'
    args_num_steps = 1024
    args_cuda = False
    args_use_gae = False
    args_gamma = 1.0
    args_tau = 0.95
    args_ppo_epoch = 4
    args_num_mini_batch = 32
    args_save_interval = 50
    args_log_interval = args_save_interval
    args_save_dir = 'trained_models'
    args_num_processes = 1
    args_env_name = 'quadCopter_pre'
    maxIter = 1000
    # construct the environment and start training
    environment = QuadCopEnv
    envs = [make_env(environment, args_seed, i, args_log_dir) for i in range(1)]
    envs = DummyVecEnv(envs)
    envs.envs[0].env.encourage = True
    envs.envs[0].env.discourage = True
    env = envs.envs[0].env  # token
    envs = VecNormalize(envs, False, False, -np.inf, np.inf, 1.0, 1e-8)  # not sure what eps does
    obs_shape = envs.observation_space.shape
    obs_shape = (obs_shape[0], *obs_shape[1:])
    rollouts = RolloutStorage(args_num_steps, 1, obs_shape, envs.action_space, 1)
    current_obs = torch.zeros(args_num_processes, *obs_shape)
    # we are ready to train
    def update_current_obs(obs):
        shape_dim0 = envs.observation_space.shape[0]
        obs = torch.from_numpy(obs).float()
        current_obs[:, -shape_dim0:] = obs

    obs = envs.reset()
    update_current_obs(obs)

    rollouts.observations[0].copy_(current_obs)

    # These variables are used to compute average rewards for all processes.
    episode_rewards = torch.zeros([args_num_processes, 1])
    final_rewards = torch.zeros([args_num_processes, 1])
    record_final_rewards = [[] for _ in range(args_num_processes)]

    if args_cuda:
        current_obs = current_obs.cuda()
        rollouts.cuda()
    # setting for iteration process
    # start iteration
    start = time.time()
    for j in range(maxIter):
        for step in range(args_num_steps):  # collect enough data
            # Sample actions
            value, action, action_log_prob, states = ppo.act(Variable(rollouts.observations[step], volatile=True),
                                                                      Variable(rollouts.states[step], volatile=True),
                                                                      Variable(rollouts.masks[step], volatile=True))
            cpu_actions = action.data.squeeze(1).cpu().numpy()
            if cpu_actions.ndim == 1:
                cpu_actions = np.expand_dims(cpu_actions, axis=0)

            # Obser reward and next obs
            obs, reward, done, info = envs.step(cpu_actions)
            reward = torch.from_numpy(np.expand_dims(np.stack(reward), 1)).float()
            episode_rewards += reward

            # If done then clean the history of observations.
            masks = torch.FloatTensor([[0.0] if done_ else [1.0] for done_ in done])
            final_rewards *= masks
            final_rewards += (1 - masks) * episode_rewards
            episode_rewards *= masks
            for i, frwd in enumerate(final_rewards):
                if done[i]:
                    record_final_rewards[i].append(frwd[0])

            if args_cuda:
                masks = masks.cuda()

            if current_obs.dim() == 4:
                current_obs *= masks.unsqueeze(2).unsqueeze(2)
            else:
                current_obs *= masks

            update_current_obs(obs)
            rollouts.insert(step, current_obs, states.data, action.data, action_log_prob.data, value.data, reward, masks)
        # print latest 10 % of rewards, its mean, min, max, etc
        for rec in record_final_rewards:
            lenrec = len(rec)
            if lenrec > 0:
                recdt = np.array(rec[int(0.9*lenrec):lenrec])
                rmean, rstd, rmin, rmax = recdt.mean(), recdt.std(), recdt.min(), recdt.max()
                sys.stdout.write('\rAt Iter {}, mean/std {}, min/max/median {}'.format(j, [rmean, rstd], [rmin, rmax]))
                sys.stdout.flush()
        # calculate value function at next step using the observation
        next_value = ppo.getVA(Variable(rollouts.observations[-1], volatile=True))[0].data

        rollouts.compute_returns(next_value, args_use_gae, args_gamma, args_tau)  # calculate returns

        # extract data and do iterations
        diffRet = rollouts.returns[:-1] - rollouts.value_preds[:-1]
        for e in range(args_ppo_epoch):
            data_generator = rollouts.feed_forward_generator(diffRet, args_num_mini_batch)
            ppo.batch_update(data_generator)
        # prepare for next step
        rollouts.after_update()
        if (j + 1) % args_save_interval == 0 and args_save_dir != "":
            save_path = os.path.join(args_save_dir, 'ppo')
            try:
                os.makedirs(save_path)
            except OSError:
                pass

            # A really ugly way to save a model to CPU
            save_model = ppo
            if args_cuda:
                save_model = copy.deepcopy(ppo).cpu()

            save_model = [save_model, record_final_rewards, hasattr(envs, 'ob_rms') and envs.ob_rms or None]

            # torch.save(save_model, os.path.join(save_path, args_env_name + ".pt"))
            torch.save(save_model, mdlName)

        if (j + 1) % args_log_interval == 0:
            end = time.time()
            total_num_steps = (j + 1) * args_num_processes * args_num_steps
            print("Updates {}, num timesteps {}, FPS {}, mean/median reward {:.1f}/{:.1f}, min/max reward {:.1f}/{:.1f}".
                format(j, total_num_steps,
                       int(total_num_steps / (end - start)),
                       final_rewards.mean(),
                       final_rewards.median(),
                       final_rewards.min(),
                       final_rewards.max()))


def supervisedTrain(args):
    """Use supervised approach to train a good network"""
    environ = env()
    # solve lqr problem, to find K and S
    Q = environ.Q
    R = environ.R
    A = np.zeros((2, 2))  # lqr for each component
    A[0, 1] = 1
    B = np.array([[0, 1]]).T
    vK = np.zeros((3, 1, 2))  # I know its size
    vS = np.zeros((3, 2, 2))
    for i in range(3):
        if i < 2:
            iQ = np.diag(np.array([Q[i], Q[2 + i]]))
        else:
            iQ = np.diag(Q[4:])
        iR = np.array([[R[0]]])
        K, S, E = lqr(A, B, iQ, iR)
        vK[i] = K
        vS[i] = S
    # start training process
    # get env info
    x0mean = (environ.xlb + environ.xub) / 2.0
    x0std = (environ.xub - environ.xlb)/np.sqrt(12.0)
    x0mean = np.concatenate((x0mean[:4], [0.5, 0.5], x0mean[-1:]))
    x0std = np.concatenate((x0std[:4], [0.5, 0.5], x0std[-1:]))
    # build the network is the first thing to do
    ppo = sPPO(environ)
    ppo.addActNet([aLyr])
    ppo.addVNet([vLyr])
    ppo.actNet.setScale([x0mean, x0std], None)
    ppo.oldActNet = copy.deepcopy(ppo.actNet)
    ppo.valueNet.setScale([x0mean, x0std])
    # train the networks, using some heuristics
    preTrain(ppo, environ, vK, vS)
    saveModel(ppo, preMdlName)


def preTrain(ppo, environ, vK, vS):
    # we define a sampler and generator to generate a bunch of training data
    alr = 1e-3
    vlr = 1e-3
    epoch = 1000
    batchsize = 32
    # get two optimizer
    aOpt = optim.Adam(ppo.actNet.parameters(), lr=alr)
    vOpt = optim.Adam(ppo.valueNet.parameters(), lr=vlr)
    # assume we train several epochs
    for epch_ in range(epoch):
        samples = (environ.xub - environ.xlb)*np.random.uniform(size=(batchsize, environ.dimx)) + environ.xlb
        X, V, U = genVU(samples, vK, vS)
        # do optimization here
        # X = (X - x0mean) / x0std, this is not necessary since it's moved
        fds = Variable(torch.from_numpy(X).float(), requires_grad=False)
        fdV = Variable(torch.from_numpy(V).float(), requires_grad=False)
        fdU = Variable(torch.from_numpy(U).float(), requires_grad=False)
        predV = ppo.valueNet(fds)
        predU = ppo.actNet.forward(fds, determ=True)
        errorV = (predV - fdV)
        errorU = (predU - fdU)
        lossV = F.smooth_l1_loss(predV, fdV).mean()
        lossU = F.smooth_l1_loss(predU, fdU).mean()
        aOpt.zero_grad()
        lossU.backward()
        aOpt.step()
        vOpt.zero_grad()
        lossV.backward()
        vOpt.step()
        # report progress in both errors
        sys.stdout.write('\rEpoch {} lossV {} lossU {}'.format(epch_, lossV.data[0], lossU.data[0]))
        sys.stdout.flush()
    print('\nTraining process finished')


def genVU(samples, vK, vS):
    # get V and U for those samples
    dof = 3
    batchsize = samples.shape[0]
    vU = np.zeros((batchsize, dof))
    vV = np.zeros((batchsize, dof))
    for i in range(dof):
        if i < 2:
            sx = np.vstack((samples[:, i], samples[:, i + 2])).T
        else:
            sx = samples[:, -2:]
        vU[:, i] = np.squeeze(-sx.dot(vK[i].T))  # drop dimension
        vV[:, i] = sx[:, 0]**2 * vS[i, 0, 0] + sx[:, 1]**2 * vS[i, 1, 1] + sx[:, 0]*sx[:, 1] * (vS[i, 0, 1] + vS[i, 1, 0])
        # for j in range(batchsize):
        #    vV[j, i] = sx[j, :].dot(vS[i].dot(dx[j, :]))
    vU[:, 0] += 1.0  # due to gravity compensation
    # clip ctrl
    vU[:, 0] = np.clip(vU[:, 0], -2, 2)
    vU[:, 1] = np.clip(vU[:, 1], -2, 2)
    vU[:, 2] = np.clip(vU[:, 2], -1, 1)
    # use some least square trick to find control, sum to get V
    V = np.sum(vV, axis=1, keepdims=True)
    U = np.zeros((batchsize, 2))  # 2 since we have 2 controls
    # construct the LS matrice
    cth = np.cos(samples[:, 4])  # since theta is 5-th element
    sth = np.sin(samples[:, 4])  # I use the fact that (X^T X)^{-1}X^T = 1/2*[[cth, sth, -1], [cth, sth, 1]]
    col1 = 0.5*(vU[:, 0]*cth + vU[:, 1]*sth)
    col2 = 0.5*vU[:, 2]
    U[:, 0] = col1 - col2
    U[:, 1] = col1 + col2
    X = np.concatenate((samples[:, :4], sth[:, np.newaxis], cth[:, np.newaxis], samples[:, -1:]), axis=1)
    return X, V, U


def saveModel(ppo, name):
    """Save the class instance here"""
    torch.save(ppo, name)


def loadModel(name):
    return torch.load(name)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser('Control how we use pretrained model to boost convergence')
    parser.add_argument('--supervise', '-sv', action='store_true', default=False, help='Enable to supervised pre-training')
    parser.add_argument('--learn', '-l', action='store_true', default=False, help='Enable to use ppo to train a policy')
    parser.add_argument('--cont', '-ct', action='store_true', default=False, help='Enable to continue training')
    parser.add_argument('--scratch', action='store_true', default=False, help='Enable to learn RL from scratch')
    args = parser.parse_args()
    main(args)
