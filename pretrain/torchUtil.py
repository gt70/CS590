#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
torchUtil.py

Utility function associated with torch
"""
import torch
import torch.nn as nn


class GaoNet(nn.Module):
    def __init__(self, lyrs, outdim=None, dropout=None):
        super(GaoNet, self).__init__()
        self.layers = []
        if outdim is None:
            nHidden = len(lyrs) - 2
            lasthidden = lyrs[-2]
            outlayer = lyrs[-1]
        else:
            nHidden = len(lyrs) - 1
            lasthidden = lyrs[-1]
            outlayer = outdim
        for i in range(nHidden):
            self.layers.append(nn.Linear(lyrs[i], lyrs[i+1]))
            self.layers.append(nn.LeakyReLU(0.2))
            if i > 0 and dropout is not None:
                self.layers.append(nn.Dropout(p=dropout))
        # final layer is linear output
        self.layers.append(nn.Linear(lasthidden, outlayer))
        # use the OrderedDict trick to assemble the system
        self.main = nn.Sequential(OrderedDict([(str(i), lyr) for i, lyr in enumerate(self.layers)]))
        # for m in self.modules():
        #     if isinstance(m, nn.Linear):
        #         m.bias.data.zero_()
        #         m.weight.data.normal_(0, 0.1)

    def forward(self, x):
        out = x
        for i, lyr in enumerate(self.layers):
            out = lyr(out)
        return out

    def getWeights(self):
        vw, vb = [], []
        for m in self.modules():
            if isinstance(m, nn.Linear):
                vw.append(m.weight.data.numpy())
                vb.append(m.bias.data.numpy())
        return {'w': vw, 'b': vb}
