# policygradient
This folder contains the code to do the project for CS590, reinforcement learning.
The goal is to use reinforcement learning algorithms to solve vehicle control problems. 

# model.py
Include models written in gym style such that I can directly render/call them. 
I might want to include several functions to generate initial state / generate ctrl / check okay. 

# utility.py
As always, many useful functions to finish this task. 

# reinforce.py
My implementation of reinforce algorithm to control a system. 

# actor_critic.py
My implementation of the actor-critic algorithm based on the example of PyTorch

# sppo.py
Implementation of sppo algorithm in a class. s means single, it is not parallelizable now. 

# sppotest.py
Use pendulum problem to test sppo

# sppopen.py
Test the problem on known pendulum swingup problem. It might be difficult to solve. 

# distributions.py
Copied. Implement simple distribution functions. 

# externalmodel.py
Include the car problem. 

# sppoDubin.py
Use sppo to solve Dubincar problem. 

# qPen.py
Use q-learning to do pendulum swing-up problem. 

# qDubinCar.py
Use q-learning to solve the dubin car problem. 

# qDiscretePen.py
For debug only, solve the simplest pendulum to check algorithm. 

# checkWithOCP.py
Use traditional methods to solve the OCP and check if it outperforms qlearning.

# utils.py
Copied. Utilities
