#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
sppoDubin.py

test if the sppo is written correctly by using the peudulum problem
"""
from sppo import sPPO
import gym
from externalmodel import DubinCarEnv, oneDBug
from model import knownPendulum
import torch
from torch.autograd import Variable
import numpy as np
import matplotlib.pyplot as plt
import cPickle as pkl


DEBUG = 0
# env = DubinCarEnv() 
# pklName = 'sppoDubin.pkl'
# env = knownPendulum()
env = gym.make("Pendulum-v0")
pklName = 'sppoPen.pkl'
env.seed(13)


def main(cont):
    if not cont:
        ppo = sPPO(env)
        actLyr = [100]
        valueLyr = [100]
        ppo.addActNet(actLyr)
        ppo.addVNet(valueLyr)
    else:
        print('We continue training')
        with open(pklName, 'rb') as f:
            ppo = pkl.load(f)
    maxIter = 100
    saveFreq = 20
    maxTrajLen = 1024
    gamma = 0.99
    lam = 0.95
    batchsize = 64  # batch size for training
    optim_epochs = 10
    numbatch = maxTrajLen / batchsize

    allIterV, allIterLen = [], []
    itr = 0
    # collect a bunch of trajectories
    seg_gen = traj_segment_generator(ppo, env, maxTrajLen, True)
    while True:
        seg = seg_gen.next()
        add_vtarg_and_adv(seg, gamma, lam)
        # get a bunch of data
        ob, ac, atarg, tdlamret = seg["ob"], seg["ac"], seg["adv"], seg["tdlamret"]
        vpredbefore = seg["vpred"] # predicted value function before udpate
        atarg = (atarg - atarg.mean()) / atarg.std() # standardized advantage function estimate
        # copy net
        ppo.copyNet()
        for _ in range(optim_epochs):
            lenpnt = len(ob)
            rdind = np.random.shuffle(np.arange(lenpnt))
            ob, ac, atarg, tdlamret = ob[rdind], ac[rdind], atarg[rdind], tdlamret[rdind]
            # iterate over the dataset
            for batch in range(numbatch):
                id0, idf = batch * batchsize, (batch + 1) * batchsize
                vob = ob[id0:idf]
                vac = ac[id0:idf]
                vatarg = atarg[id0:idf]
                vvtarg = tdlamret[id0:idf]

                feeds = Variable(torch.from_numpy(vob).float(), requires_grad=False)
                feedr = Variable(torch.from_numpy(vatarg).float(), requires_grad=False)
                feeda = Variable(torch.from_numpy(vac).float(), requires_grad=False)
                feedv = Variable(torch.from_numpy(vvtarg).float(), requires_grad=False)
                # calculate surrogate L
                newProb = ppo.actNet.getProb(feeds, feeda)
                oldProb = ppo.oldActNet.getProb(feeds, feeda)
                ratio = newProb / oldProb
                surr = ratio * feeda
                surr2 = torch.clamp(ratio, 1.0 - ppo.clip, 1.0 + ppo.clip) * feeda
                aloss = -torch.mean(torch.min(surr, surr2))
                # calculate loss function in value function
                predv = ppo.valueNet(feeds)
                vloss = (predv - feedv).pow(2).mean()
                loss = aloss + vloss
                if DEBUG:
                    print('closs = {}'.format(vloss.data[0]))
                    if np.isnan(vloss.data.numpy()):
                        print('Nan loss detected')
                ppo.valueOptim.zero_grad()
                ppo.actOptim.zero_grad()
                loss.backward()
                ppo.valueOptim.step()
                ppo.actOptim.step()
        # post analysis of data
        if (itr + 1) % saveFreq == 0:
            with open(pklName, 'wb') as f:
                pkl.dump(ppo, f)
        avgepret = np.mean(seg['ep_rets'])
        avgeplen = np.mean(seg['ep_lens'])
        if itr == 0:
            allIterV.append(avgepret)
        else:
            allIterV.append(allIterV[-1] * 0.9 + avgepret * 0.1)  # smooth the reward 
        if not np.isnan(avgeplen):
            allIterLen.append(int(avgeplen))
        print("Avg return {} avg len {}".format(avgepret, avgeplen))
        if itr >= maxIter:
            print("We finished all iterations, exit")
            break
        itr += 1

    # simulation of learned policy
    # raw_input('Press Enter to view simulation')
    while True:
        s = env.reset()
        env.render()
        state0 = env.state.copy()
        for t in range(300):
            a = ppo.chooseAction(s, True)
            s, r, done, _ = env.step(a)
            env.render()
            if done:
                break
        print('Start from {}, End at {}'.format(state0, env.state))


def add_vtarg_and_adv(seg, gamma, lam):
    """
    Compute target value using TD(lambda) estimator, and advantage with GAE(lambda)
    """
    new = np.append(seg["new"], 0) # last element is only used for last vtarg, but we already zeroed it if last new = 1
    vpred = np.append(seg["vpred"], seg["nextvpred"])
    T = len(seg["rew"])
    seg["adv"] = gaelam = np.empty(T, 'float32')
    rew = seg["rew"]
    lastgaelam = 0
    for t in reversed(range(T)):
        nonterminal = 1-new[t+1]
        delta = rew[t] + gamma * vpred[t+1] * nonterminal - vpred[t]
        gaelam[t] = lastgaelam = delta + gamma * lam * nonterminal * lastgaelam
    seg["tdlamret"] = seg["adv"] + seg["vpred"]


def traj_segment_generator(ppo, env, horizon, stochastic):
    t = 0
    ac = env.action_space.sample() # not used, just so we have the datatype
    new = True # marks if we're on first timestep of an episode
    ob = env.reset()

    cur_ep_ret = 0 # return in current episode
    cur_ep_len = 0 # len of current episode
    ep_rets = [] # returns of completed episodes in this segment
    ep_lens = [] # lengths of ...

    # Initialize history arrays
    obs = np.array([ob for _ in range(horizon)])  # observations
    rews = np.zeros(horizon, 'float32')  # rewards
    vpreds = np.zeros(horizon, 'float32')  # value predictions
    news = np.zeros(horizon, 'int32')  # if we are new
    acs = np.array([ac for _ in range(horizon)])  # actions
    prevacs = acs.copy()

    while True:
        prevac = ac
        vpred, ac = ppo.getVA(ob, not stochastic)
        # Slight weirdness here because we need value function at time T
        # before returning segment [0, T-1] so we get the correct
        # terminal value
        if t > 0 and t % horizon == 0:
            yield {"ob" : obs, "rew" : rews, "vpred" : vpreds, "new" : news,
                    "ac" : acs, "prevac" : prevacs, "nextvpred": vpred * (1 - new),
                    "ep_rets" : ep_rets, "ep_lens" : ep_lens}
            # Be careful!!! if you change the downstream algorithm to aggregate
            # several of these batches, then be sure to do a deepcopy
            ep_rets = []
            ep_lens = []
        i = t % horizon
        obs[i] = ob
        vpreds[i] = vpred
        news[i] = new
        acs[i] = ac
        prevacs[i] = prevac

        ob, rew, new, externalreward = env.step(ac)
        rews[i] = rew

        cur_ep_ret += rew
        cur_ep_len += 1
        if new:
            ep_rets.append(cur_ep_ret)
            ep_lens.append(cur_ep_len)
            cur_ep_ret = 0
            cur_ep_len = 0
            ob = env.reset()
        t += 1


def showModel(n=100, render=True):
    with open(pklName, 'rb') as f:
        ppo = pkl.load(f)
    # loop for some times
    loopNum = n
    vsf = []
    for loop in range(loopNum):
        s = env.reset()
        if render:
            env.render()
        print('Start from {}'.format(env.state))
        for t in range(300):
            a = ppo.chooseAction(s, True)
            s, r, done, _ = env.step(a)
            if render:
                env.render()
            if done:
                break
        vsf.append(env.state)
    vsf = np.array(vsf)
    distsf = np.linalg.norm(vsf, axis=1)
    print(np.histogram(distsf))
    print(vsf)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', action='store_true', default=False, help='run simulation')
    parser.add_argument('-cont', '-c', action='store_true', default=False, help='continue training')
    args = parser.parse_args()
    if args.s:
        showModel(n=10, render=True)
    else:
        main(args.cont)
