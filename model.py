#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ===============================================================================
#
#         FILE: model.py
#
#  DESCRIPTION: based on openai gym, define our custom systems
# ===============================================================================
import gym
import numpy as np
from os import path
from gym import spaces
from gym.utils import seeding
from gym.envs.classic_control import rendering


class pendulumEnv(gym.Env):
    metadata = {
        'render.modes' : ['human', 'rgb_array'],
        'video.frames_per_second' : 30
    }

    def __init__(self):
        self.max_speed=8
        self.max_torque=2.
        self.dt=.1
        self.viewer = None

        high = np.array([1., 1., self.max_speed])
        self.action_space = spaces.Box(low=-self.max_torque, high=self.max_torque, shape=(1,))
        self.observation_space = spaces.Box(low=-high, high=high)
        self.last_u = None

        self._seed()

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _step(self,u):
        th, thdot = self.state # th := theta
        th = angle_normalize(th)

        g = 10.
        m = 1.
        l = 1.
        dt = self.dt

        u = np.clip(u, -self.max_torque, self.max_torque)[0]
        self.last_u = u # for rendering
        costs = th**2 + .1*thdot**2 + .001*(u**2)

        newthdot = thdot + (-3*g/(2*l) * np.sin(th + np.pi) + 3./(m*l**2)*u) * dt
        newth = th + newthdot*dt
        newthdot = np.clip(newthdot, -self.max_speed, self.max_speed) #pylint: disable=E1111

        self.state = np.array([newth, newthdot])
        done = False
        if np.linalg.norm(self.state) < 0.2:
            done = True
        return self._get_obs(), -costs, done, {}

    def _reset(self):
        # high = np.array([np.pi, 1])
        # self.state = self.np_random.uniform(low=-high, high=high)
        self.state = np.array([np.pi/2.0, 0])
        self.last_u = None
        return self._get_obs()

    def _get_obs(self):
        theta, thetadot = self.state
        return np.array([np.cos(theta), np.sin(theta), thetadot])

    def _render(self, mode='human', close=False):
        if close:
            if self.viewer is not None:
                self.viewer.close()
                self.viewer = None
            return

        if self.viewer is None:
            self.viewer = rendering.Viewer(500,500)
            self.viewer.set_bounds(-2.2,2.2,-2.2,2.2)
            rod = rendering.make_capsule(1, .2)
            rod.set_color(.8, .3, .3)
            self.pole_transform = rendering.Transform()
            rod.add_attr(self.pole_transform)
            self.viewer.add_geom(rod)
            axle = rendering.make_circle(.05)
            axle.set_color(0,0,0)
            self.viewer.add_geom(axle)

        if self.last_u is not None:
            line = rendering.Line((1.7, 0), (1.7, self.last_u))
            line.set_color(0, 0, 0)
            self.viewer.add_onetime(line)
        self.pole_transform.set_rotation(self.state[0] - np.pi/2)

        return self.viewer.render(return_rgb_array = mode=='rgb_array')

def angle_normalize(x):
    return (((x+np.pi) % (2.0*np.pi)) - np.pi)


class knownPendulum(pendulumEnv):
    def __init__(self, unwrap=False):
        pendulumEnv.__init__(self)
        self.max_torque = 1.0
        self.MAXSPEED = 3.0
        self.dt = .1
        self.viewer = None
        self.tfweight = 1.0
        self.R = np.array([1])
        self.Q = np.array([0, 0])

        high = np.array([1., 1., self.MAXSPEED])
        self.action_space = spaces.Box(low=-self.max_torque, high=self.max_torque, shape=(1,))
        if not unwrap:
            low = np.array([0, -self.MAXSPEED])
            high = np.array([2*np.pi, self.MAXSPEED])
            self.observation_space = spaces.Box(low=low, high=high)
        else:
            self.observation_space = spaces.Box(low=-high, high=high)

        self.x0lb = np.array([0, -2.0])
        self.x0ub = np.array([np.pi, 2.0])
        self.dimx, self.dimu = 2, 1

        self._seed()
        self.state = None
        self.unwrap = unwrap

        # if we encourage and discourage
        self.encourage = True
        self.discourage = True

    def _step(self, u):
        u = np.clip(u, self.action_space.low, self.action_space.high)
        th, thdot = self.state # th := theta
        # th = angle_normalize(th)
        newth = th + thdot * self.dt
        newthdot = thdot + self.dt * (u[0] - np.sin(th))
        self.last_u = u[0]
        self.state = np.array([newth, newthdot])
        done = False
        dtheta = np.mod(newth, 2*np.pi) - np.pi
        #if np.abs(newth) > 4*np.pi or np.abs(newthdot) > 5:
            # done = True
        #    cost = 100
        #else:
        cost = (self.tfweight + np.sum(u**2*self.R) + self.Q[0]*dtheta**2+self.Q[1]*newthdot**2) * self.dt
        if np.abs(newth) > 4*np.pi or np.abs(newthdot) > 5:
            done = True
            if self.discourage:
                cost += 100
        if np.linalg.norm([dtheta, newthdot]) < 0.1:
            done = True
            if self.encourage:
                cost += -100
        return self._get_obs(), -cost, done, {}

    def _reset(self):
        self.state = np.random.uniform(size=self.dimx) * (self.x0ub - self.x0lb) + self.x0lb
        # self.state = np.array([0.0, 0.0])
        self.last_u = None
        return self._get_obs()

    def _get_obs(self):
        theta, thetadot = self.state
        thetadot = np.clip(thetadot, -self.MAXSPEED, self.MAXSPEED)
        if self.unwrap:
            return np.array([np.cos(theta), np.sin(theta), thetadot])
        else:
            return np.array([np.mod(theta, 2.0*np.pi), thetadot])


class discretePendulum(pendulumEnv):
    def __init__(self, unwrap=True):
        pendulumEnv.__init__(self)
        self.max_torque = 1.
        self.MAXSPEED = 3.0
        self.dt = .1
        self.viewer = None
        self.tfweight = 1.0
        self.R = np.array([1])

        high = np.array([1., 1., self.MAXSPEED])
        self.action_space = spaces.Discrete(3)
        self.observation_space = spaces.Box(low=-high, high=high)

        self.x0lb = np.array([0, -2.0])
        self.x0ub = np.array([np.pi, 2.0])
        self.dimx, self.dimu = 2, 1

        self._seed()
        self.state = None
        self.unwrap = unwrap

    def _step(self, u):
        th, thdot = self.state # th := theta
        # th = angle_normalize(th)
        if u == 0:
            ctrl = -self.max_torque
        elif u == 1:
            ctrl = 0
        else:
            ctrl = self.max_torque
        newth = th + thdot * self.dt
        newthdot = thdot + self.dt * (ctrl - np.sin(th))
        newth = np.mod(newth + np.pi, 2*np.pi) - np.pi
        self.last_u = ctrl
        self.state = np.array([newth, newthdot])
        done = False
        dtheta = newth - np.pi
        rewardFunc = -(dtheta)**2 + -0.25*(newthdot ** 2) 
        #if np.abs(newth) > 4*np.pi or np.abs(newthdot) > 5:
        #    done = True
        #    rewardFunc = -100
        return self._get_obs(), rewardFunc, done, {}

    def _reset(self):
        # self.state = np.array([0.0, 0.0])
        self.state = np.random.random(size=2)*(self.x0ub - self.x0lb) + self.x0lb
        self.last_u = None
        return self._get_obs()

    def _get_obs(self):
        theta, thetadot = self.state
        thetadot = np.clip(thetadot, -self.MAXSPEED, self.MAXSPEED)
        if self.unwrap:
            return np.array([np.cos(theta), np.sin(theta), thetadot])
        else:
            return np.array([np.mod(theta, 2.0*np.pi), thetadot])


def main():
    pass


if __name__ == '__main__':
    main()
