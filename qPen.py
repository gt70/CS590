#! /usr/bin/env python
import numpy as np
import math
import random
import gym
import cPickle as pkl
import matplotlib.pyplot as plt
from model import knownPendulum
# from utility import centralSlice, getIndex

## Initialize the "pendulum" environment
env = knownPendulum(False)
# env = gym.make('Pendulum-v0')

## Defining the environment related constants

# Number of discrete states (bucket) per state dimension
NUM_BUCKETS = (120, 60)  # (theta, theta')
NUM_ACTIONS = env.action_space.low.size
NUM_ACTION_BUCKETS = (10,)  # make it smaller
# Bounds for each discrete state
LOW_BOUNDS = [0.0, -3.]
HIGH_BOUNDS = [2*math.pi, 3.]
LOW_U_BOUNDS = env.action_space.low
HIGH_U_BOUNDS = env.action_space.high
# Index of the action
ACTION_INDEX = len(NUM_BUCKETS)


def centralSlice(low, high, N):
    """given low and high, slice into N evenly spaced values"""
    slc1 = np.linspace(low, high, N + 1)
    return (slc1[:-1] + slc1[1:]) / 2.0


def getIndex(slc, val):
    if isinstance(val, np.ndarray) and isinstance(slc, list):
        ind = [getIndex(slc_, val_) for slc_, val_ in zip(slc, val)]
        return tuple(np.array(ind, dtype=np.int))
    else:
        assert np.isscalar(val)
        ind = int((val - slc[0]) / (slc[1] - slc[0]))
        return ind


## Creating a Q-Table for each state-action pair
q_table = np.ones(NUM_BUCKETS + NUM_ACTION_BUCKETS) * 0
vSlice = [centralSlice(low, high, bucket) for low, high, bucket in zip(LOW_BOUNDS, HIGH_BOUNDS, NUM_BUCKETS)]
uSlice = [centralSlice(low, high, bucket) for low, high, bucket in zip(LOW_U_BOUNDS, HIGH_U_BOUNDS, NUM_ACTION_BUCKETS)]

## Defining the simulation related constants
NUM_EPISODES = 5000
MAX_T = 200
STREAK_TO_END = 120
SOLVED_T = 199
DEBUG_MODE = False
FILENAME = 'qPenTenU.pkl'

## Learning related constants
minEpsilon = 0.01
epsilonDecay = 0.99
MIN_LEARNING_RATE = 0.1
MAX_LEARNING_RATE = 0.3
M_L = MIN_LEARNING_RATE * NUM_EPISODES / (MAX_LEARNING_RATE - MIN_LEARNING_RATE)
N_L = MAX_LEARNING_RATE * M_L
MIN_EXPLORE_RATE = 0.05
MAX_EXPLORE_RATE = 0.5
M_E = MIN_EXPLORE_RATE * NUM_EPISODES / (MAX_EXPLORE_RATE - MIN_EXPLORE_RATE)
N_E = MAX_EXPLORE_RATE * M_L


def main(args):
    if args.learn:
        learn(args.cont)
    if args.show:
        show()
        # showOne(np.array([2, -2]))
    if args.plot:
        plotQ()


def rewardFun(x):
    if x.ndim == 1:
        return -(x[0] - np.pi) ** 2 - 0.25*x[1] ** 2
    else:
        return -(x[:, 0] - np.pi) ** 2 - 0.25 * x[:, 1] ** 2


def learn(cont):
    global q_table
    # mat1, mat2 = np.meshgrid(vSlice[0], vSlice[1], indexing='ij')
    # pair = np.stack((mat1.flatten(), mat2.flatten()), axis=1)
    # R = rewardFun(pair)
    # R = np.reshape(R, NUM_BUCKETS)
    if cont:
        with open(FILENAME, 'rb') as f:
            q_table = pkl.load(f)
        EXPLORE_RATE = 0.05
    else:
        # for k in range(q_table.shape[2]):
        #     q_table[:, :, k] = R + 0.01*np.random.normal(size=R.shape)
        EXPLORE_RATE = 0.5
    ## Instantiating the learning related parameters
    learning_rate = get_learning_rate(0)
    EXPLORE_RATE = get_explore_rate(0)
    # explore_rate = get_explore_rate(0)
    discount_factor = 0.9  # since the world is unchanging
    targetind = getIndex(vSlice, np.array([np.pi, 0]))
    q_table[targetind] = 0

    num_streaks = 0

    for episode in range(NUM_EPISODES):

        # Reset the environment
        obv = env.reset()

        # the initial state
        ind0 = getIndex(vSlice, obv)

        for t in range(MAX_T):
            # env.render()

            # Select an action
            actind = select_action(ind0, EXPLORE_RATE)
            action = np.array([uSlice[i][actind[i]] for i in range(NUM_ACTIONS)])

            # Execute the action
            obv, reward, done, _ = env.step(action)

            # Setting up for the next iteration
            ind1 = getIndex(vSlice, obv)
            # reward = R[ind1]
            #if ind0[0] == targetind[0] and ind0[1] == targetind[1]:
            #    done = True
            #if np.linalg.norm([obv[0] - np.pi, obv[1]]) < 0.01:
            #    bonus = 100
            #    done = True
            #else:
            #    bonus = 0
            bonus = 0
                # Update the Q based on the result
            best_q = np.amax(q_table[ind1])
            q_table_ind = tuple(np.append(ind0, actind).tolist())
            q_table[q_table_ind] += learning_rate * (reward + discount_factor * (best_q) - q_table[q_table_ind] + bonus)
            ind0 = ind1
            # Print data
            if (DEBUG_MODE):
                print("\nEpisode = %d" % episode)
                print("t = %d" % t)
                print("Action: %d" % action)
                print("State: %s" % str(state))
                print("Reward: %f" % reward)
                print("Best Q: %f" % best_q)
                print("Explore rate: %f" % explore_rate)
                print("Learning rate: %f" % learning_rate)
                print("Streaks: %d" % num_streaks)

                print("")

            if done:
               print("Episode %d finished after %f time steps" % (episode, t))
               if (t >= SOLVED_T):
                   num_streaks += 1
               else:
                   num_streaks = 0
               break

            #sleep(0.25)

        # It's considered done when it's solved over 120 times consecutively
        if num_streaks > STREAK_TO_END:
            break

        # Update parameters
        # explore_rate = get_explore_rate(episode)
        # if EXPLORE_RATE > minEpsilon:
        #     EXPLORE_RATE *= epsilonDecay
        learning_rate = get_learning_rate(episode)
        EXPLORE_RATE = get_explore_rate(episode)
    with open(FILENAME, 'wb') as f:
        pkl.dump(q_table, f)


def oneSim(x0, render=False):
    # load the table
    global q_table
    with open(FILENAME, 'rb') as f:
        q_table = pkl.load(f)
    if x0.ndim == 1:
        x0 = np.expand_dims(x0, axis=0)
    # simulate along those
    vRst = []
    for x0_ in x0:
        vX, vU = [], []
        env.state = x0_  # manually assign states
        vX.append(x0_)
        obv = env._get_obs()
        ind0 = getIndex(vSlice, obv)
        for t in range(MAX_T):
            if render:
                env.render()
            # Select an action
            actind = select_action(ind0, 0.0)  # always use optimal q
            action = np.array([uSlice[i][actind[i]] for i in range(NUM_ACTIONS)])
            vU.append(action)

            # Execute the action
            obv, reward, done, _ = env.step(action)
            vX.append(env.state)
            # Setting up for the next iteration
            ind0 = getIndex(vSlice, obv)
            if done:
                break
        # assemble to get one result
        rst = {'vX': np.array(vX), 'vU': np.array(vU), 'vt': np.arange(0, len(vX))*env.dt}
        if x0.shape[0] == 1:
            return rst
        else:
            vRst.append(rst)
    return vRst


def show(numTrial=100, rendering=False):
    global q_table
    with open(FILENAME, 'rb') as f:
        q_table = pkl.load(f)
    """Use the qtable we learned and show how it performs"""
    oknum = 0
    for i in range(numTrial):
        obv = env.reset()
        obv0 = obv
        # the initial state
        ind0 = getIndex(vSlice, obv)

        for t in range(MAX_T):
            if rendering:
                env.render()

            # Select an action
            actind = select_action(ind0, 0.0)  # always use optimal q
            action = np.array([uSlice[i][actind[i]] for i in range(NUM_ACTIONS)])

            # Execute the action
            obv, reward, done, _ = env.step(action)

            # Setting up for the next iteration
            ind0 = getIndex(vSlice, obv)

            # Print data
            if (DEBUG_MODE):
                print("t = %d, state = %s, action = %s" % (t, str(obv), str(action)))

            if done:
                oknum += 1
                break
        print('Simulation {} from {} to {}'.format(i, obv0, obv))
    print('{} out of {} succeed'.format(oknum, numTrial))


def showOne(x0):
    global q_table
    with open(FILENAME, 'rb') as f:
        q_table = pkl.load(f)
    """Use the qtable we learned and show how it performs"""
    env.state = x0
    obv = env._get_obs()
    obv0 = obv
    # the initial state
    ind0 = getIndex(vSlice, obv)

    for t in range(MAX_T):
        env.render()

        # Select an action
        actind = select_action(ind0, 0.0)  # always use optimal q
        action = np.array([uSlice[i][actind[i]] for i in range(NUM_ACTIONS)])

        # Execute the action
        obv, reward, done, _ = env.step(action)

        # Setting up for the next iteration
        ind0 = getIndex(vSlice, obv)

        # Print data
        if (DEBUG_MODE):
            print("t = %d, state = %s, action = %s" % (t, str(obv), str(action)))

        if done:
            break
        print(t, env.state, action)
    print('Simulation {} from {} to {}'.format(i, obv0, obv))


def plotQ():
    global q_table
    with open(FILENAME, 'rb') as f:
        q_table = pkl.load(f)
    """Use the qtable we learned and plot the policy function"""
    thslice, thdotslice = vSlice
    thX, thdotY = np.meshgrid(thslice, thdotslice, indexing='ij')
    mat = np.zeros(NUM_BUCKETS)
    for i in range(NUM_BUCKETS[0]):
        for j in range(NUM_BUCKETS[1]):
            tb = q_table[i, j]
            uind = np.argmax(tb)
            u = uSlice[0][uind]
            mat[i, j] = u
    fig, ax = plt.subplots()
    cax = ax.contourf(thX, thdotY, mat, cmap=plt.cm.jet)
    ax.set_xlabel(r'$\theta$')
    ax.set_ylabel(r'$\omega$')
    fig.colorbar(cax)
    fig.savefig('pendulum_u.pdf')
    # plot the optimal value function for each state
    fig, ax = plt.subplots()
    V = np.amax(q_table, axis=2)
    cax = ax.contourf(thX, thdotY, V, cmap=plt.cm.jet)
    ax.set_xlabel(r'$\theta$')
    ax.set_ylabel(r'$\omega$')
    # ax.set_title(r'Optimal Q')
    fig.colorbar(cax)
    fig.savefig('pendulum_Q.pdf')
    plt.show()


def select_action(ind0, explore_rate):
    # Select a random action
    if random.random() < explore_rate:
        actind = tuple([np.random.randint(sz) for sz in NUM_ACTION_BUCKETS])
    # Select the action with the highest q
    else:
        if(DEBUG_MODE):
            table = q_table[ind0]
        actind = np.argmax(q_table[ind0], axis=-1)
        if not isinstance(actind, np.ndarray):
            actind = tuple([actind])
        else:
            actind = tuple(actind.tolist())
    return actind


def get_explore_rate(t):
    # return 0.05
    #return MAX_EXPLORE_RATE - (MAX_EXPLORE_RATE - MIN_EXPLORE_RATE) * float(t) / float(NUM_EPISODES)
    #return max(MIN_EXPLORE_RATE, min(1.0, 1.0 - math.log10((float(t)+1)/25.0)))
    return N_E / (M_E + t)


def get_learning_rate(t):
    # return 0.2
    #return MAX_LEARNING_RATE - (MAX_LEARNING_RATE - MIN_LEARNING_RATE) * float(t) / float(NUM_EPISODES)
    # return max(MIN_LEARNING_RATE, min(0.5, 1.0 - math.log10((float(t)+1)/25.0)))
    return N_L / (M_L + t)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-learn', '-l', action='store_true', default=False, help='learn it')
    parser.add_argument('-show', '-s', action='store_true', default=False, help='show it')
    parser.add_argument('-cont', '-c', action='store_true', default=False, help='continue')
    parser.add_argument('-plot', '-p', action='store_true', default=False, help='plot policy')
    args = parser.parse_args()
    main(args)
