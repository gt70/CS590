#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
Implementation of actor-critic algorithm for reinforcement learning.
"""
import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from utility import GaoNet
import gym
from model import pendulumEnv
from collections import OrderedDict
import numpy as np
import matplotlib.pyplot as plt
from reinforce import postPlot


def main(args):
    if args.show:
        checkPlot()
    else:
        simpleAC()


class ACNet(nn.Module):
    """the PolicyNet stores its state whenever evaluated"""
    def __init__(self, indim, outdim, nhidden):
        # initialize the neural network
        super(ACNet, self).__init__()
        self.layers = []
        assert len(nhidden) > 0
        lenhdn = len(nhidden)
        if len(nhidden) > 0:
            self.layers.append(nn.Linear(indim, nhidden[0]))
        for i in xrange(lenhdn - 1):
            self.layers.append(nn.Linear(nhidden[i], nhidden[i + 1]))
            self.layers.append(nn.LeakyReLU(0.2))
        # last one
        self.lastulayer = nn.Linear(nhidden[-1], outdim)
        self.lastvlayer = nn.Linear(nhidden[-1], 1)
        # use the OrderedDict trick to assemble the system
        alllayers = self.layers + [self.lastulayer, self.lastvlayer]
        self.main = nn.Sequential(OrderedDict([(str(i), lyr) for i, lyr in enumerate(alllayers)]))
        # self.train()
        # initialize stored
        self.saved_action = []
        self.reward = []

    def forward(self, x):
        out = x
        for i, lyr in enumerate(self.layers):
            out = lyr(out)
        # predict the control
        outu = self.lastulayer(out)
        outu = torch.nn.functional.tanh(outu) * 2  # rescale to [-2, 2]
        outv = self.lastvlayer(out)  # assume it is unbounded
        return outu, outv

    def selectAct(self, obs, determ=False):
        obs = torch.from_numpy(obs).float().unsqueeze(0)
        umean, v = self.__call__(Variable(obs))
        if not determ:
            u = torch.normal(means=umean, std=0.02)
        else:
            u = umean
        self.saved_action.append((u, v))
        return u.data.numpy().squeeze(axis=0)

    def addReward(self, r):
        self.reward.append(r)

    def update(self, gamma, optimizer):
        """Update the neural network after one run"""
        R = 0
        lenrwd = len(self.reward)
        Rewards = np.zeros(lenrwd)
        for i in xrange(lenrwd):
            r = self.reward[-1 - i]
            R = r + gamma * R
            Rewards[-1 - i] = R
        Rewards = torch.from_numpy(Rewards)
        Rewards = (Rewards - Rewards.mean()) / (Rewards.std() + np.finfo(np.float32).eps)
        # updatet neural networks
        vloss = 0
        for (action, value), r in zip(self.saved_action, Rewards):
            rwd = r - value.data[0, 0]
            action.reinforce(rwd)
            vloss += torch.nn.functional.smooth_l1_loss(value, Variable(torch.Tensor([r])))
        optimizer.zero_grad()
        allnodes = [vloss] + [act[0] for act in self.saved_action]
        grads = [torch.ones(1)] + [None] * len(self.saved_action)
        torch.autograd.backward(allnodes, grads)
        optimizer.step()

    def reset(self):
        del self.saved_action[:]
        del self.reward[:]


def checkPlot():
    hist = np.load('hist.npz')
    postPlot(hist['histt'], hist['histx'], hist['histu'])


def simpleAC():
    """Implementation of the simple reinforce algorithm."""
    env = pendulumEnv()
    dimx, dimo, dimu = 2, 3, 1
    # construct a simple network, give deterministic ctrl
    net = ACNet(dimo, dimu, [50])
    gamma = 0.99
    optimizer = optim.Adam(net.parameters(), lr=1e-3)
    allEpisode = 1000
    episodeStep = 500
    donetimes = 0
    maxdonetimes = 200
    histRewards = []
    largestReward = -1e10
    for epsd in xrange(allEpisode):
        if epsd % 100 == 0:
            print('episode = {}'.format(epsd))
        # generate an episode
        obs = env.reset()
        for step in xrange(episodeStep):
            action = net.selectAct(obs)
            obs, reward, done, _ = env.step(action)
            net.addReward(reward)
            if reward > largestReward:
                largestReward = reward
                print('Largest reward so far is {}'.format(largestReward))
            if done:
                donetimes += 1
                break
        histRewards.append(np.sum(net.reward))
        net.update(gamma, optimizer)
        net.reset()
        if donetimes > maxdonetimes:
            print('We have done {} times'.format(donetimes))
            break
    print('We reach done {} times'.format(donetimes))
    fig, ax = plt.subplots()
    ax.plot(histRewards)
    fig.savefig('Learning Process')
    # try to replay
    maxstep = 50
    hist = forwardEvaluate(env, net, maxstep, render=False)
    postPlot(hist['histt'], hist['histx'], hist['histu'])


def forwardEvaluate(env, net, maxstep, render=False):
    obs = env.reset()
    vstate = []
    vctrl = []
    vstate.append(env.state)
    if render:
        env.render()
    for _ in xrange(maxstep):
        action = net.selectAct(obs, True)
        vctrl.append(action)
        obs, cost, done, _ = env.step(action)
        if render:
            env.render()
        vstate.append(env.state)
        if done:
            print('Succeed')
            break
    histx = np.array(vstate)
    histt = env.dt * np.arange(len(histx))
    histu = np.array(vctrl)
    rst = {'histt': histt, 'histx': histx, 'histu': histu}
    np.savez('hist.npz', **rst)
    return rst


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-show', action="store_true", default=False)
    args = parser.parse_args()
    main(args)
