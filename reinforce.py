#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ===============================================================================
#
#         FILE: reinforce.py
#
#  DESCRIPTION: Implementation of the reinforce algorithm to control a system
# ===============================================================================
import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
from utility import GaoNet
import gym
from model import pendulumEnv
from collections import OrderedDict
import numpy as np
import matplotlib.pyplot as plt


def main():
    simpleReinforce()
    # checkPlot()


class PolicyNet(nn.Module):
    """the PolicyNet stores its state whenever evaluated"""
    def __init__(self, indim, outdim, nhidden):
        # initialize the neural network
        super(PolicyNet, self).__init__()
        self.layers = []
        assert len(nhidden) > 0
        lenhdn = len(nhidden)
        if len(nhidden) > 0:
            self.layers.append(nn.Linear(indim, nhidden[0]))
        for i in xrange(lenhdn - 1):
            self.layers.append(nn.Linear(nhidden[i], nhidden[i + 1]))
            self.layers.append(nn.LeakyReLU(0.2))
        # last one
        self.layers.append(nn.Linear(nhidden[-1], outdim))
        # use the OrderedDict trick to assemble the system
        self.main = nn.Sequential(OrderedDict([(str(i), lyr) for i, lyr in enumerate(self.layers)]))
        # initialize stored
        self.saved_action = []
        self.reward = []

    def reset(self):
        self.saved_action = []
        self.reward = []

    def forward(self, x):
        out = x
        for i, lyr in enumerate(self.layers):
            out = lyr(out)
        out = torch.nn.functional.tanh(out) * 2
        return out

    def selectAct(self, obs, determ=False):
        obs = torch.from_numpy(obs).float().unsqueeze(0)
        mean = self.__call__(Variable(obs))
        if not determ:
            u = torch.normal(means=mean, std=0.02)
        else:
            u = mean
        self.saved_action.append(u)
        return u.data.numpy().squeeze(axis=0)

    def addReward(self, r):
        self.reward.append(r)

    def update(self, gamma, optimizer):
        """Update the neural network based on action"""
        R = 0
        lenrwd = len(self.reward)
        Rewards = np.zeros(lenrwd)
        for i in xrange(lenrwd):
            r = self.reward[-1 - i]
            R = r + gamma * R
            Rewards[-1 - i] = R
        Rewards = torch.from_numpy(Rewards)
        Rewards = (Rewards - Rewards.mean()) / (Rewards.std() + np.finfo(np.float32).eps)
        for action, r in zip(self.saved_action, Rewards):
            action.reinforce(r)
        optimizer.zero_grad()
        torch.autograd.backward(self.saved_action, [None for _ in self.saved_action])
        optimizer.step()


def checkPlot():
    hist = np.load('hist.npz')
    postPlot(hist['histt'], hist['histx'], hist['histu'])


def simpleReinforce():
    """Implementation of the simple reinforce algorithm."""
    env = pendulumEnv()
    dimx, dimo, dimu = 2, 3, 1
    # construct a simple network, give deterministic ctrl
    net = PolicyNet(dimo, dimu, [50])
    gamma = 0.99
    optimizer = optim.Adam(net.parameters(), lr=5e-3)
    allEpisode = 1000
    episodeStep = 500
    donetimes = 0
    histRewards = []
    for epsd in xrange(allEpisode):
        if epsd % 100 == 0:
            print('episode = {}'.format(epsd))
        # generate an episode
        obs = env.reset()
        for step in xrange(episodeStep):
            action = net.selectAct(obs)
            obs, cost, done, _ = env.step(action)
            net.addReward(cost)
            if done:
                donetimes += 1
                break
        histRewards.append(np.sum(net.reward))
        net.update(gamma, optimizer)
        net.reset()
    print('We reach done {} times'.format(donetimes))
    fig, ax = plt.subplots()
    ax.plot(histRewards)
    fig.savefig('Learning Process')
    # try to replay
    maxstep = 50
    hist = forwardEvaluate(env, net, maxstep, render=False)
    postPlot(hist['histt'], hist['histx'], hist['histu'])


def forwardEvaluate(env, net, maxstep, render=False):
    obs = env.reset()
    vstate = []
    vctrl = []
    vstate.append(env.state)
    if render:
        env.render()
    for _ in xrange(maxstep):
        action = net.selectAct(obs)
        vctrl.append(action)
        obs, cost, done, _ = env.step(action)
        if render:
            env.render()
        vstate.append(env.state)
        if done:
            print('Succeed')
            break
    histx = np.array(vstate)
    histt = env.dt * np.arange(len(histx))
    histu = np.array(vctrl)
    rst = {'histt': histt, 'histx': histx, 'histu': histu}
    np.savez('hist.npz', **rst)
    return rst


def postPlot(histt, histx, histu):
    """plot the history"""
    fig, ax = plt.subplots()
    dimx = histx.shape[1]
    dimu = histu.shape[1]
    for i in xrange(dimx):
        ax.plot(histt, histx[:, i])
    for i in xrange(dimu):
        ax.plot(histt[:-1], histu[:, i], linestyle='--')
    plt.show()


if __name__ == '__main__':
    main()
