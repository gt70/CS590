#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ===============================================================================
#
#         FILE: utility.py
#
#  DESCRIPTION: utility function to be called elsewhere
# ===============================================================================

import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F
# import torch.distributions as dist
from GAO.RL.CS590.distributions import DiagGaussian, PureGaussian
import numpy as np
from collections import OrderedDict
import pickle as pickle
import math


DEBUG = 1


class gaussianLyr(nn.Module):
    """A layer that takes in mean, maintain std for predictiton"""
    def __init__(self, nvar):
        super(gaussianLyr, self).__init__()
        self.nvar = nvar
        self.std = nn.Parameter(torch.zeros(nvar))

    def forward(self, x):
        std = self.std.expand_as(x)
        std = F.softplus(std)
        return torch.normal(means=x, std=std)

    def prob(self, mu, output):
        std = F.softplus(self.std)
        den = (2*math.pi)**(self.nvar/2.0)*torch.prod(std)
        num = -0.5*torch.sum(((output - mu)/std).pow(2), 1, keepdim=True)
        prob = num.exp_() / den
        return prob


class GaoNet(nn.Module):
    def __init__(self, lyrs=None, outdim=None, net=None, xScale=None, yScale=None):
        super(GaoNet, self).__init__()
        self.multiplier = 1.0
        self.offset = 0.0
        if net is None:
            self.layers = []
            if outdim is None:
                nHidden = len(lyrs) - 2
                lasthidden = lyrs[-2]
                outlayer = lyrs[-1]
            else:
                nHidden = len(lyrs) - 1
                lasthidden = lyrs[-1]
                outlayer = outdim
            for i in range(nHidden):
                self.layers.append(nn.Linear(lyrs[i], lyrs[i+1]))
                self.layers.append(nn.LeakyReLU(0.2))
            # final layer is linear
            self.layers.append(nn.Linear(lasthidden, outlayer))
            # use the OrderedDict trick to assemble the system
            self.main = nn.Sequential(OrderedDict([(str(i), lyr) for i, lyr in enumerate(self.layers)]))
            self.netdict = None
        else:
            self.netdict = net  # a net dict
            self.netmodel = net['model']
            xScale = net.get('xScale', None)
            yScale = net.get('yScale', None)
        self.setScale(xScale, yScale)

    def setScale(self, xScale=None, yScale=None):
        self.xScale = xScale
        self.yScale = yScale
        # update scale factors
        if self.xScale is not None:
            if isinstance(self.xScale[0], np.ndarray):
                self.xmean = torch.from_numpy(self.xScale[0]).float()
                self.xstd = torch.from_numpy(self.xScale[1]).float()
            else:
                self.xmean, self.xstd = self.xScale
        if self.yScale is not None:
            if isinstance(self.yScale[0], np.ndarray):
                self.ymean = torch.from_numpy(self.yScale[0]).float()
                self.ystd = torch.from_numpy(self.yScale[1]).float()
            else:
                self.ymean, self.ystd = self.yScale

    def forward(self, x):
        if self.xScale is not None:
            x = (x - Variable(self.xmean, requires_grad=False)) / Variable(self.xstd, requires_grad=False)
        if self.netdict is None:
            out = x
            for i, lyr in enumerate(self.layers):
                out = lyr(out)
        else:
            out = self.netmodel(x)
        if self.yScale is not None:
            out = out * Variable(self.ystd, requires_grad=False) + Variable(self.ymean, requires_grad=False)
        return out * self.multiplier + self.offset

    def eval(self, x):
        if isinstance(x, np.ndarray):
            if x.ndim == 1:
                inx = np.expand_dims(x, axis=0)
            else:
                inx = x
            fdx = Variable(torch.from_numpy(inx).float(), requires_grad=False)
            out = self.forward(fdx).data.numpy()
            if isinstance(x, np.ndarray) and x.ndim == 1:
                out = np.squeeze(out, axis=0)
        else:
            out = self.forward(x)
        return out


class GaoBiasNet(GaoNet):
    """Final guassian is not connected with states but still changable"""
    def __init__(self, lyrs=None, outdim=None, net=None):
        GaoNet.__init__(self, lyrs, outdim, net)
        # final mean layer
        # self.mulayer = nn.Linear(lasthidden, outdim)  # for mu
        self.mutanhlayer = nn.Tanh()  # get [-1, 1]
        self.stdlayer = gaussianLyr(outdim)

    def forward(self, x, determ=False):
        mu = GaoNet.forward(self, x)
        # apply to get mu and std
        if self.netdict is None:  # if not well-trained model, we might need this
        #    mu = self.mutanhlayer(self.mulayer(mu))
            mu = self.mutanhlayer(mu)
        if determ:
            return mu
        else:
            return self.stdlayer(mu)

    def eval(self, x, determ=True):
        if isinstance(x, np.ndarray):
            if x.ndim == 1:
                inx = np.expand_dims(x, axis=0)
            else:
                inx = x
            fdx = Variable(torch.from_numpy(inx).float(), requires_grad=False)
        else:
            fdx = x
        out = self.forward(fdx, determ)
        if isinstance(x, np.ndarray):
            out = out.data.numpy() 
            if x.ndim == 1:
                out = np.squeeze(out, axis=0)
        return out

    def getProb(self, x, a):
        """Get probability when input is x and output is a"""
        mu = GaoNet.forward(self, x)
        # apply to get mu and std
        if self.netdict is None:  # if not well-trained model, we might need this
            mu = self.mutanhlayer(mu)
        prob = self.stdlayer.prob(mu, a)
        return prob


# obsolete class and is not suggested to be used
class GaoActNet(nn.Module):
    """Input a layer, output a distribution. """
    def __init__(self, lyrs, outdim, ubd=None, fixstd=None):
        assert len(ubd) == 2 and len(ubd[0]) == outdim and len(ubd[1]) == outdim
        super(GaoActNet, self).__init__()
        self.layers = []
        nHidden = len(lyrs) - 1
        lasthidden = lyrs[-1]
        for i in range(nHidden):
            self.layers.append(nn.Linear(lyrs[i], lyrs[i+1]))
            # self.layers.append(nn.LeakyReLU(0.2))
            self.layers.append(nn.ReLU())
        # final two layers use
        self.mulayer = nn.Linear(lasthidden, outdim)  # for mu
        self.mutanhlayer = nn.Tanh()  # get [-1, 1]
        alllyrs = self.layers + [self.mulayer]
        self.fixstd = fixstd
        if fixstd is None:
            self.stdlayer = nn.Linear(lasthidden, outdim)
            self.stdSoftlayer = nn.Softplus()
            alllyrs.append(self.stdlayer)
        else:
            self.fixstd = Variable(torch.from_numpy(self.fixstd).float(), volatile=False)
        self.normallayer = PureGaussian()
        self.main = nn.Sequential(OrderedDict([(str(i), lyr) for i, lyr in enumerate(alllyrs)]))
        self.ubd = ubd
        self.umid = Variable(torch.from_numpy((ubd[1] + ubd[0]) / 2.0).float().unsqueeze(0), volatile=False)
        self.uhalf = Variable(torch.from_numpy((ubd[1] - ubd[0]) / 2.0).float().unsqueeze(0), volatile=False)

    def forward(self, x, determ=False):
        out = x
        for i, lyr in enumerate(self.layers):
            out = lyr(out)
        # print('out = {}'.format(out))
        # apply to get mu and std
        mu = self.mutanhlayer(self.mulayer(out))
        # move mu to the bound
        mu = mu * self.uhalf + self.umid
        if self.fixstd is None:
            std = self.stdSoftlayer(self.stdlayer(out)) + 1e-5
        else:
            std = self.fixstd
        if DEBUG == 1:
            print('mu = {}, std = {}'.format(mu, std))
        return self.normallayer(mu, std)

    def getProb(self, x, a):
        """Get probability of x when output is a"""
        out = x
        for i, lyr in enumerate(self.layers):
            out = lyr(out)
        mu = self.mutanhlayer(self.mulayer(out))
        mu = mu * self.uhalf + self.umid
        if self.fixstd is None:
            std = self.stdSoftlayer(self.stdlayer(out)) + 1e-5
        else:
            std = self.fixstd
        return self.normallayer.prob(mu, std, a)
        # calculate mu
        # mu = F.tanh(self.mulayer(out))
        # umu = mu * self.uhalf + self.umid
        # if self.fixstd is None:
        #     std = F.softplus(self.stdlayer(out))
        # else:
        #     std = self.fixstd
        # return dist.Normal(mean=umu, std=std)


def centralSlice(low, high, N):
    """given low and high, slice into N evenly spaced values"""
    slc1 = np.linspace(low, high, N + 1)
    return (slc1[:-1] + slc1[1:]) / 2.0


def getIndex(slc, val):
    if isinstance(val, np.ndarray) and isinstance(slc, list):
        ind = [getIndex(slc_, val_) for slc_, val_ in zip(slc, val)]
        return tuple(np.array(ind, dtype=np.int))
    else:
        assert np.isscalar(val)
        ind = int((val - slc[0]) / (slc[1] - slc[0]))
        return ind


def main():
    pass


if __name__ == '__main__':
    main()
