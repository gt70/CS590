#! /usr/bin/env python
import torch
import numpy as np
import math
import random
import gym
import cPickle as pkl
import matplotlib.pyplot as plt
from model import knownPendulum
from externalmodel import DubinCarEnv
from qPen import centralSlice, getIndex
import sys

## Initialize the "pendulum" environment
env = DubinCarEnv(False)

## Defining the environment related constants

# Number of discrete states (bucket) per state dimension
NUM_BUCKETS = (5, 5, 10, 10)  # (x, y, theta, v)
NUM_ACTIONS = env.action_space.low.size
NUM_ACTION_BUCKETS = (5, 5)  # make it smaller
# Bounds for each discrete state
LOW_BOUNDS = env.xlb
HIGH_BOUNDS = env.xub
LOW_U_BOUNDS = np.zeros_like(env.ulb) + 1
HIGH_U_BOUNDS = -LOW_U_BOUNDS

## Creating a Q-Table for each state-action pair
q_table = np.ones(NUM_BUCKETS + NUM_ACTION_BUCKETS) * 0
vSlice = [centralSlice(low, high, bucket) for low, high, bucket in zip(LOW_BOUNDS, HIGH_BOUNDS, NUM_BUCKETS)]
uSlice = [centralSlice(low, high, bucket) for low, high, bucket in zip(LOW_U_BOUNDS, HIGH_U_BOUNDS, NUM_ACTION_BUCKETS)]

## Learning related constants
minEpsilon = 0.01
epsilonDecay = 0.99
MIN_LEARNING_RATE = 0.1
MAX_LEARNING_RATE = 0.5
MIN_EXPLORE_RATE = 0.05
MAX_EXPLORE_RATE = 0.5

## Defining the simulation related constants
NUM_EPISODES = 100000
MAX_T = 150
DEBUG_MODE = False
FILENAME = 'qDubinCar.pkl'


def main(args):
    if args.learn:
        learn(args.cont)
    if args.show:
        show()


def learn(cont):
    global q_table
    # Instantiating the learning related parameters
    learning_rate = get_learning_rate(0)
    EXPLORE_RATE = get_explore_rate(0)
    # explore_rate = get_explore_rate(0)
    discount_factor = 0.99  # since the world is unchanging
    targetind = getIndex(vSlice, np.array([0, 0, 0, 0]))
    q_table[targetind] = 0
    if cont:
        with open(FILENAME, 'rb') as f:
            q_table = pkl.load(f)

    freq = NUM_EPISODES // 100
    for episode in range(NUM_EPISODES):
        if (episode + 1) % freq == 0:
            sys.stdout.write('\r{} out of {} finished'.format(episode, NUM_EPISODES))
            sys.stdout.flush()
        # Reset the environment
        obv = env.reset()
        # the initial state
        ind0 = getIndex(vSlice, obv)
        for t in range(MAX_T):
            # env.render()
            # Select an action
            actind = select_action(ind0, EXPLORE_RATE)
            action = np.array([uSlice[i][actind[i]] for i in range(NUM_ACTIONS)])
            # Execute the action
            obv, reward, done, _ = env.step(action)
            # Setting up for the next iteration
            ind1 = getIndex(vSlice, obv)
            if np.allclose(targetind, ind0):
                done = True
                bonus = 100
                print("Episode %d finished after %d time steps" % (episode, t))
            else:
                bonus = 0
                # Update the Q based on the result
            best_q = np.amax(q_table[ind1])
            q_table_ind = tuple(np.append(ind0, actind).tolist())
            q_table[q_table_ind] += learning_rate * (reward + discount_factor * best_q - q_table[q_table_ind] + bonus)
            ind0 = ind1
            # Print data
            if (DEBUG_MODE):
                print("\nEpisode = %d" % episode)
                print("t = %d" % t)
                print("Action: %d" % action)
                print("State: %s" % str(obv))
                print("Reward: %f" % reward)
                print("Best Q: %f" % best_q)

            if done:
               # print("Episode %d finished after %f time steps" % (episode, t))
               break

            #sleep(0.25)

        # Update parameters
        # explore_rate = get_explore_rate(episode)
        # if EXPLORE_RATE > minEpsilon:
        #     EXPLORE_RATE *= epsilonDecay
        learning_rate = get_learning_rate(episode)
        EXPLORE_RATE = get_explore_rate(episode)
    with open(FILENAME, 'wb') as f:
        pkl.dump(q_table, f)


def show(numTrial=100):
    global q_table
    with open(FILENAME, 'rb') as f:
        q_table = pkl.load(f)
    """Use the qtable we learned and show how it performs"""
    targetind = getIndex(vSlice, np.array([0, 0, 0, 0]))
    donenum = 0
    for i in range(numTrial):
        obv = env.reset()
        obv0 = obv
        # the initial state
        ind0 = getIndex(vSlice, obv)

        for t in range(MAX_T):
            env.render()
            # Select an action
            actind = select_action(ind0, 0.0)  # always use optimal q
            action = np.array([uSlice[i][actind[i]] for i in range(NUM_ACTIONS)])
            print('state is {} action is {}'.format(env.state, action))
            # Execute the action
            obv, reward, done, _ = env.step(action)
            # Setting up for the next iteration
            ind0 = getIndex(vSlice, obv)
            if np.allclose(targetind, ind0):
                done = True
                donenum += 1
                print('All set')
            # Print data
            if (DEBUG_MODE):
                print("t = %d, state = %s, action = %s" % (t, str(obv), str(action)))
            if done:
                break
        print('\nSimulation {} from {} to {}'.format(i, obv0, obv))
    print('{} / {} succeed'.format(donenum, numTrial))


def select_action(ind0, explore_rate):
    # Select a random action
    if random.random() < explore_rate:
        actind = tuple([np.random.randint(sz) for sz in NUM_ACTION_BUCKETS])
    # Select the action with the highest q
    else:
        if(DEBUG_MODE):
            table = q_table[ind0]
        submat = q_table[ind0]
        actind = np.unravel_index(submat.argmax(), submat.shape)
    return actind


def get_explore_rate(t):
    return MAX_EXPLORE_RATE - (MAX_EXPLORE_RATE - MIN_EXPLORE_RATE) * float(t) / float(NUM_EPISODES)


def get_learning_rate(t):
    return MAX_LEARNING_RATE - (MAX_LEARNING_RATE - MIN_LEARNING_RATE) * float(t) / float(NUM_EPISODES)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-learn', '-l', action='store_true', default=False, help='learn it')
    parser.add_argument('-show', '-s', action='store_true', default=False, help='show it')
    parser.add_argument('-cont', '-c', action='store_true', default=False, help='continue')
    args = parser.parse_args()
    main(args)
