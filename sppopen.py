#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
sppopen.py

See how sppo performs for the pendulum swingup problem with penalty on states
"""
from sppo import sPPO
import gym
import numpy as np
import matplotlib.pyplot as plt
import cPickle as pkl
from model import knownPendulum


DEBUG = 0
pklName = 'sppoPen.pkl'


def main(cont):
    env = knownPendulum()
    env.discourage = False
    if not cont:
        ppo = sPPO(env)
        actLyr = [100]
        valueLyr = [100]
        ppo.addActNet(actLyr)
        ppo.addVNet(valueLyr)
    else:
        with open(pklName, 'rb') as f:
            ppo = pkl.load(f)
    maxIter = 1500
    saveFreq = 100
    maxTrajLen = 100
    gamma = 0.999
    batch = 32  # batch size for training

    allIterV = []
    for itr in range(maxIter):
        s = env.reset()
        vs, va, vr = [], [], []
        sumr = 0
        for t in range(maxTrajLen):
            a = ppo.chooseAction(s)
            if DEBUG == 1:
                print('s = {} a = {}'.format(s, a))
            s_, r, done, _ = env.step(a)
            vs.append(s)
            va.append(a)
            vr.append((r + 8) / 8)  # hack suggested by others
            s = s_
            sumr += r
            # update ppo, occasionally
            if (t + 1) % batch == 0 or t == maxTrajLen - 1 or done:
                v_s_ = ppo.getVA(s_)[0]
                discR = np.zeros_like(vr)
                lenvr = len(vr)
                for i in range(lenvr):
                    v_s_ = vr[-1 - i] + gamma * v_s_
                    discR[-1 - i] = v_s_
                # ready to feed into ppo for update
                vecs, veca, vecr = np.vstack(vs), np.vstack(va), discR[:, np.newaxis]
                ppo.update(vecs, veca, vecr)
                vs, va, vr = [], [], []
            if done:
                break
        if (itr + 1) % saveFreq == 0:
            with open(pklName, 'wb') as f:
                pkl.dump(ppo, f)
        if itr == 0:
            allIterV.append(sumr)
        else:
            allIterV.append(allIterV[-1] * 0.9 + sumr * 0.1)  # do not know why we need this
        print('iter: {} sumR: {}'.format(itr, sumr))

    # plot the training process
    plt.plot(np.arange(len(allIterV)), allIterV)
    plt.xlabel('Episode');plt.ylabel('Moving averaged episode reward');plt.show()


def plotField(pklName=pklName):
    """Load the trained model and plot the field information"""
    with open(pklName, 'rb') as f:
        ppo = pkl.load(f)
    # create a grid of theta and vel
    nAgl, nVel = 100, 200
    vecAngle = np.linspace(0, 2*np.pi, nAgl)
    vecVel = np.linspace(-2, 2, nVel)
    gridx, gridy = np.meshgrid(vecAngle, vecVel, indexing='ij')
    mValue = np.zeros_like(gridx)
    mPolicy = np.zeros_like(gridx)
    for i, agl in enumerate(vecAngle):
        sini, cosi = np.sin(agl)*np.ones(nVel), np.cos(agl)*np.ones(nVel)
        usex = np.stack([cosi, sini, vecVel], axis=1)
        value, act = ppo.getVA(usex)
        mValue[i, :] = value.squeeze(axis=1)
        mPolicy[i, :] = act.squeeze(axis=1)
    # plot two figures
    fig, axs = plt.subplots()
    cax0 = axs.contourf(gridx, gridy, mValue, cmap=plt.cm.jet)
    axs.set_xlabel(r'$\theta$')
    axs.set_ylabel(r'$\omega$')
    axs.set_title(r'value function')
    fig.colorbar(cax0)
    fig, axs = plt.subplots()
    cax1 = axs.contourf(gridx, gridy, mPolicy, cmap=plt.cm.jet)
    axs.set_xlabel(r'$\theta$')
    axs.set_ylabel(r'$\omega$')
    axs.set_title(r'policy')
    fig.colorbar(cax1)
    plt.show()


def showModel(n, render=True, pklName=pklName):
    env = knownPendulum()
    with open(pklName, 'rb') as f:
        ppo = pkl.load(f)
    # loop for some times
    loopNum = n 
    for loop in range(loopNum):
        s = env.reset()
        vsf = []
        vsf.append(env.state)
        if render:
            env.render()
        for t in range(200):
            a = ppo.chooseAction(s, True)
            s, c, done, _ = env.step(a)
            if render:
                env.render()
            if done:
                break
        vsf.append(env.state)
        print('Start from {} end at {}'.format(vsf[0], vsf[-1]))
    vsf = np.array(vsf)
    distsf = np.linalg.norm(vsf, axis=1)
    print(np.histogram(distsf))
    print vsf


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', action='store_true', default=False)
    parser.add_argument('-cont', '-c', action='store_true', default=False)
    parser.add_argument('-plot', '-p', action='store_true', default=False)
    args = parser.parse_args()
    if args.s:
        showModel(100, True)
    elif args.plot:
        plotField()
    else:
        main(args.cont)
