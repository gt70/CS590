#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
#
# Copyright © 2017 Gao Tang <gt70@duke.edu>
#
# Distributed under terms of the MIT license.

"""
sppo.py
Single process version of proximal policy optimization

Based on someone's tensorflow implementation. 
"""
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable

import numpy as np
import gym
from GAO.RL.CS590.utility import GaoNet, GaoBiasNet

import copy
import sys


DEBUG = 0


class sPPO(object):
    def __init__(self, env):
        self.clip = 0.1  # clip parameter
        self.alr = 2e-4  # learning rate for action
        self.vlr = 2e-4  # learning rate for value
        self.aUpdateStep = 4
        self.cUpdateStep = 4
        self.max_grad_norm = 0.5
        self.dimO = env.observation_space.shape[0]  # dimension of obs
        self.dimA = env.action_space.shape[0]  # dimension of action
        self.umid = (env.action_space.low + env.action_space.high) / 2.0
        self.uhalf = (env.action_space.high - env.action_space.low) / 2.0
        self.Tumid = torch.from_numpy(self.umid[np.newaxis, :]).float()
        self.Tuhalf = torch.from_numpy(self.uhalf[np.newaxis, :]).float()

    def batch_update(self, data_generator):
        for sample in data_generator:
            obs_batch, states_batch, actions_batch, return_batch, masks_batch, \
                    old_action_log_probs_batch, adv_targ = sample
            feeds = Variable(obs_batch, requires_grad=False)
            feedr = Variable(return_batch, requires_grad=False)
            usea = Variable(actions_batch, requires_grad=False)
            # feeda = (usea - Variable(self.Tumid, requires_grad=False)) / Variable(self.Tuhalf, requires_grad=False)
            feeda = (usea - Variable(self.Tumid, requires_grad=False)) / Variable(self.Tuhalf, requires_grad=False)
            adv = Variable(adv_targ, requires_grad=True)
            # get adv loss
            newProb = self.actNet.getProb(feeds, feeda)
            oldProb = self.oldActNet.getProb(feeds, feeda)
            ratio = newProb / oldProb
            surr = ratio * adv
            surr2 = torch.clamp(ratio, 1.0 - self.clip, 1.0 + self.clip) * adv
            loss = -torch.mean(torch.min(surr, surr2)) 
            # loss on value function
            predv = self.valueNet(feeds)
            vloss = (predv - feedr).pow(2).mean()
            # optimize
            self.actOptim.zero_grad()
            self.valueOptim.zero_grad()
            ttlloss = loss + vloss
            ttlloss.backward()
            self.actOptim.step()
            self.valueOptim.step()
        self.copyNet()
        return

    def update(self, s, a, r):
        """Take collected s, a, r, update the networks"""
        # calculate advantage
        feeds = Variable(torch.from_numpy(s).float(), requires_grad=False)
        feedr = Variable(torch.from_numpy(r).float(), requires_grad=False)
        usea = (a - self.umid) / self.uhalf
        feeda = Variable(torch.from_numpy(usea).float(), requires_grad=False)
        # adv = (adv - adv.mean()) / (adv.std() + 1e-6)  # sometimes helpful
        # update actor
        for _ in range(self.aUpdateStep):
            predv = self.valueNet(feeds)
            adv = feedr - predv
            # normalize adv
            # adv = (adv - adv.mean()) / (adv.std() + 1e-5)
            newProb = self.actNet.getProb(feeds, feeda)
            oldProb = self.oldActNet.getProb(feeds, feeda)
            ratio = newProb / oldProb
            surr = ratio * adv
            surr2 = torch.clamp(ratio, 1.0 - self.clip, 1.0 + self.clip) * adv
            loss = -torch.mean(torch.min(surr, surr2)) 
            if DEBUG:
                print('loss = {}'.format(loss.data[0]))
                if np.isnan(loss.data.numpy()):
                    print('Nan loss detected')
                    for param in self.actNet.parameters():
                        print(param.data.numpy())
                    sys.exit(0)
            self.actOptim.zero_grad()
            loss.backward()
            # loss.backward(retain_graph=True)
            # nn.utils.clip_grad_norm(self.actNet.parameters(), self.max_grad_norm)
            self.actOptim.step()
        self.copyNet()
        # update value
        for _ in range(self.cUpdateStep):
            predv = self.valueNet(feeds)
            vloss = (predv - feedr).pow(2).mean()
            if DEBUG:
                print('closs = {}'.format(vloss.data[0]))
                if np.isnan(vloss.data.numpy()):
                    print('Nan loss detected')
            self.valueOptim.zero_grad()
            vloss.backward()
            # vloss.backward(retain_graph=True)
            self.valueOptim.step()
        return

    def act(self, obs, states, masks, deterministic=False):
        """Last two arguments are dummy, return value, action, prob, null"""
        V = self.valueNet.eval(obs)
        A = self.actNet.eval(obs, deterministic)
        if isinstance(obs, np.ndarray):
            act = self.umid + self.uhalf * A
        else:
            act = Variable(self.Tumid) + Variable(self.Tuhalf) * A
        return V, act, Variable(torch.Tensor([0])), Variable(torch.Tensor([0]))

    def addActNet(self, lyrs):  # name might be misleading, but lyrs could be a mdldict
        """Add network for calculating action"""
        if isinstance(lyrs, list):
            uselyr = [self.dimO] + lyrs
            self.actNet = GaoBiasNet(uselyr, self.dimA)
        elif isinstance(lyrs, dict):
            self.actNet = GaoBiasNet(None, self.dimA, lyrs)  # many hack but still remains elegant
        else:
            raise Exception("Not supported type")
        self.oldActNet = copy.deepcopy(self.actNet)
        self.actOptim = optim.Adam(self.actNet.parameters(), lr=self.alr)

    def copyNet(self):
        """Copy parameters from newNet to oldNet"""
        self.oldActNet.load_state_dict(self.actNet.state_dict())

    def addVNet(self, lyrs):
        """Add network for calculating value function"""
        if isinstance(lyrs, list):
            uselyr = [self.dimO] + lyrs
            self.valueNet = GaoNet(uselyr, 1)
        elif isinstance(lyrs, dict):
            self.valueNet = GaoNet(None, None, lyrs)
        else:
            raise Exception('Not supported type')
        self.valueOptim = optim.Adam(self.valueNet.parameters(), lr=self.vlr)
        # self.valueParam = self.valueNet.parameters()

    def chooseAction(self, s, determ=False):
        """Take state and return action in np array"""
        s = s[np.newaxis, :]
        act = self.actNet(Variable(torch.from_numpy(s).float()), determ)
        act = act.data.numpy().squeeze(0)
        if DEBUG == 1:
            if np.isnan(act):
                np.save('bads.npy', s)
                for param in self.actNet.parameters():
                    print(param.data)
                print('Nan detected')
        return self.umid + self.uhalf * act  # return the actual action if we want to know more

    def getVA(self, obs, determ=False):
        """Take state and return value function"""
        V = self.valueNet.eval(obs)
        A = self.actNet.eval(obs, determ)
        return V, A
